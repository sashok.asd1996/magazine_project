<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190730054240 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_users (id VARCHAR(255) NOT NULL COMMENT \'(DC2Type:user_user_id)\', date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', email VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:user_user_email)\', password_hash VARCHAR(255) DEFAULT NULL, role VARCHAR(255) NOT NULL COMMENT \'(DC2Type:user_user_role)\', status VARCHAR(16) NOT NULL, confirm_token VARCHAR(255) DEFAULT NULL, new_email VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:user_user_email)\', new_email_token VARCHAR(255) DEFAULT NULL, name_first VARCHAR(255) NOT NULL, name_last VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_F6415EB1E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_user_networks (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', user_id VARCHAR(255) NOT NULL COMMENT \'(DC2Type:user_user_id)\', network VARCHAR(32) DEFAULT NULL, identity VARCHAR(32) DEFAULT NULL, INDEX IDX_D7BAFD7BA76ED395 (user_id), UNIQUE INDEX UNIQ_D7BAFD7B608487BC6A95E9C4 (network, identity), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_order_orders (id INT AUTO_INCREMENT NOT NULL, customer_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:customer_customer_id)\', address_id INT DEFAULT NULL, grand_total NUMERIC(10, 2) NOT NULL, created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', status VARCHAR(15) NOT NULL, INDEX IDX_AB04CF509395C3F3 (customer_id), INDEX IDX_AB04CF50F5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_order_order_item (id INT AUTO_INCREMENT NOT NULL, order_id INT NOT NULL, product_id INT DEFAULT NULL, quantity SMALLINT NOT NULL, subtotal NUMERIC(10, 2) NOT NULL, INDEX IDX_6FBC86A08D9F6D38 (order_id), INDEX IDX_6FBC86A04584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_product_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, sex VARCHAR(10) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_product_image (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, category_id INT DEFAULT NULL, brand_id INT DEFAULT NULL, subcategory_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, path VARCHAR(255) NOT NULL, INDEX IDX_BE8C63F14584665A (product_id), INDEX IDX_BE8C63F112469DE2 (category_id), INDEX IDX_BE8C63F144F5D008 (brand_id), INDEX IDX_BE8C63F15DC6FE57 (subcategory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_products (id INT AUTO_INCREMENT NOT NULL, subcategory_id INT DEFAULT NULL, brand_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, price NUMERIC(10, 2) NOT NULL, sex VARCHAR(10) NOT NULL, color VARCHAR(100) NOT NULL, size VARCHAR(20) NOT NULL, season VARCHAR(50) NOT NULL, INDEX IDX_780A30CD5DC6FE57 (subcategory_id), INDEX IDX_780A30CD44F5D008 (brand_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_product_category_subcategory (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_573B21DD12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_product_brand (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_customer_address (id INT AUTO_INCREMENT NOT NULL, customer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:customer_customer_id)\', address VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, state VARCHAR(255) NOT NULL, zip_code VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, INDEX IDX_9F1FAFC39395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_customer_customers (id CHAR(36) NOT NULL COMMENT \'(DC2Type:customer_customer_id)\', email VARCHAR(255) NOT NULL COMMENT \'(DC2Type:customer_customer_email)\', mobile VARCHAR(15) NOT NULL, name_first VARCHAR(255) NOT NULL, name_last VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_user_networks ADD CONSTRAINT FK_D7BAFD7BA76ED395 FOREIGN KEY (user_id) REFERENCES user_users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_order_orders ADD CONSTRAINT FK_AB04CF509395C3F3 FOREIGN KEY (customer_id) REFERENCES product_customer_customers (id)');
        $this->addSql('ALTER TABLE product_order_orders ADD CONSTRAINT FK_AB04CF50F5B7AF75 FOREIGN KEY (address_id) REFERENCES product_customer_address (id)');
        $this->addSql('ALTER TABLE product_order_order_item ADD CONSTRAINT FK_6FBC86A08D9F6D38 FOREIGN KEY (order_id) REFERENCES product_order_orders (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_order_order_item ADD CONSTRAINT FK_6FBC86A04584665A FOREIGN KEY (product_id) REFERENCES product_products (id)');
        $this->addSql('ALTER TABLE product_product_image ADD CONSTRAINT FK_BE8C63F14584665A FOREIGN KEY (product_id) REFERENCES product_products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_product_image ADD CONSTRAINT FK_BE8C63F112469DE2 FOREIGN KEY (category_id) REFERENCES product_product_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_product_image ADD CONSTRAINT FK_BE8C63F144F5D008 FOREIGN KEY (brand_id) REFERENCES product_product_brand (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_product_image ADD CONSTRAINT FK_BE8C63F15DC6FE57 FOREIGN KEY (subcategory_id) REFERENCES product_product_category_subcategory (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_products ADD CONSTRAINT FK_780A30CD5DC6FE57 FOREIGN KEY (subcategory_id) REFERENCES product_product_category_subcategory (id)');
        $this->addSql('ALTER TABLE product_products ADD CONSTRAINT FK_780A30CD44F5D008 FOREIGN KEY (brand_id) REFERENCES product_product_brand (id)');
        $this->addSql('ALTER TABLE product_product_category_subcategory ADD CONSTRAINT FK_573B21DD12469DE2 FOREIGN KEY (category_id) REFERENCES product_product_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_customer_address ADD CONSTRAINT FK_9F1FAFC39395C3F3 FOREIGN KEY (customer_id) REFERENCES product_customer_customers (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_user_networks DROP FOREIGN KEY FK_D7BAFD7BA76ED395');
        $this->addSql('ALTER TABLE product_order_order_item DROP FOREIGN KEY FK_6FBC86A08D9F6D38');
        $this->addSql('ALTER TABLE product_product_image DROP FOREIGN KEY FK_BE8C63F112469DE2');
        $this->addSql('ALTER TABLE product_product_category_subcategory DROP FOREIGN KEY FK_573B21DD12469DE2');
        $this->addSql('ALTER TABLE product_order_order_item DROP FOREIGN KEY FK_6FBC86A04584665A');
        $this->addSql('ALTER TABLE product_product_image DROP FOREIGN KEY FK_BE8C63F14584665A');
        $this->addSql('ALTER TABLE product_product_image DROP FOREIGN KEY FK_BE8C63F15DC6FE57');
        $this->addSql('ALTER TABLE product_products DROP FOREIGN KEY FK_780A30CD5DC6FE57');
        $this->addSql('ALTER TABLE product_product_image DROP FOREIGN KEY FK_BE8C63F144F5D008');
        $this->addSql('ALTER TABLE product_products DROP FOREIGN KEY FK_780A30CD44F5D008');
        $this->addSql('ALTER TABLE product_order_orders DROP FOREIGN KEY FK_AB04CF50F5B7AF75');
        $this->addSql('ALTER TABLE product_order_orders DROP FOREIGN KEY FK_AB04CF509395C3F3');
        $this->addSql('ALTER TABLE product_customer_address DROP FOREIGN KEY FK_9F1FAFC39395C3F3');
        $this->addSql('DROP TABLE user_users');
        $this->addSql('DROP TABLE user_user_networks');
        $this->addSql('DROP TABLE product_order_orders');
        $this->addSql('DROP TABLE product_order_order_item');
        $this->addSql('DROP TABLE product_product_category');
        $this->addSql('DROP TABLE product_product_image');
        $this->addSql('DROP TABLE product_products');
        $this->addSql('DROP TABLE product_product_category_subcategory');
        $this->addSql('DROP TABLE product_product_brand');
        $this->addSql('DROP TABLE product_customer_address');
        $this->addSql('DROP TABLE product_customer_customers');
    }
}
