<?php

declare(strict_types=1);

namespace App\Controller\Profile;

use App\Model\Product\Entity\Order\Order;
use App\ReadModel\Product\OrderFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @IsGranted("ROLE_USER")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/profile/orders", name="profile.orders")
     * @param OrderFetcher $fetcher
     * @return Response
     */
    public function index(OrderFetcher $fetcher): Response
    {
        $orders = $fetcher->forCustomer($this->getUser()->getId());
        return $this->render('app/profile/orders/orders.html.twig', compact('orders'));
    }

    /**
     * @Route("/profile/orders/{id}", name="profile.orders.show")
     * @param Order $order
     * @return Response
     */
    public function show(Order $order): Response
    {
        if ($order->getCustomer()->getId() == $this->getUser()->getId()) {
            return $this->render('app/profile/orders/show.html.twig', compact('order'));
        }
        return $this->redirectToRoute('profile.orders');
    }
}