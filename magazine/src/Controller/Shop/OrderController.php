<?php

declare(strict_types=1);

namespace App\Controller\Shop;

use App\Model\Product\Entity\Customer\CustomerRepository;
use App\Model\Product\Entity\Customer\Id;
use App\Model\Product\UseCase\Cart\Show\Handler;
use App\Model\Product\UseCase\Customer\Create\Command;
use App\Model\Product\UseCase\Customer\Create\Form;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/shop/order", name="shop.order")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/checkout", name=".checkout")
     * @param Handler $handlerCart
     * @param \App\Model\Product\UseCase\Order\Create\Handler $handlerOrder
     * @param \App\Model\Product\UseCase\Customer\Create\Handler $handlerCustomer
     * @param CustomerRepository $customers
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\EntityNotFoundException
     * @throws \Exception
     */
    public function checkout
    (
        Handler $handlerCart,
        \App\Model\Product\UseCase\Order\Create\Handler $handlerOrder,
        \App\Model\Product\UseCase\Customer\Create\Handler $handlerCustomer,
        CustomerRepository $customers,
        Request $request
    ): Response
    {
        if ($this->isGranted('ROLE_USER')) {
            $session = new Session();
            if ($session->get('cart') == null) {
                return $this->redirectToRoute('shop.cart.show');
            }
            $productsInSession = $session->get('cart');
            $cart = $handlerCart->handle($productsInSession);
            if ($checkCustomer = $customers->has($customer_id = new Id($this->getUser()->getId()))) {
                $commandCustomer = Command::fromCustomer($customer = $customers->get($customer_id));
            } else {
                $commandCustomer = new Command($this->getUser()->getId());
            }
            $userName = $this->getUser()->getDisplay();
            $form = $this->createForm(Form::class, $commandCustomer);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                if (!$checkCustomer) {
                    $handlerCustomer->handle($commandCustomer);
                }
                $commandOrder = new \App\Model\Product\UseCase\Order\Create\Command($customer_id->getValue());
                $commandOrder->grandTotal = $cart['grand_total'];
                $commandOrder->products = array_unique($productsInSession);
                $commandOrder->quantity = $cart['count'];
                $commandOrder->subTotal = $cart['sub_total'];
                $commandOrder->address = $commandCustomer->address;
                $commandOrder->city = $commandCustomer->city;
                $commandOrder->country = $commandCustomer->country;
                $commandOrder->state = $commandCustomer->state;
                $commandOrder->zipCode = $commandCustomer->zipCode;

                $handlerOrder->handle($commandOrder);

                $session->set('cart', null);
                $session->set('cartCount', null);

                $this->addFlash('success', 'Your Order has been placed successfully');
                return $this->redirectToRoute('home');
            }

            return $this->render('app/shop/checkout.html.twig', [
                'cart' => $cart,
                'user' => $userName,
                'form' => $form->createView(),
            ]);
        } else {
            $this->addFlash('error', 'You need sign in');
            return $this->redirectToRoute('app_login');
        }
    }
}