<?php

declare(strict_types=1);

namespace App\Controller\Shop;

use App\Model\Product\Entity\Product\Category;
use App\Model\Product\Entity\Product\Product;
use App\ReadModel\Product\CategoryFetcher;
use App\ReadModel\Product\ForMen\Filter\Filter;
use App\ReadModel\Product\ForMen\Filter\Form;
use App\ReadModel\Product\ProductFetcher;
use App\ReadModel\Product\SubcategoryFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/shop/women", name="shop.female.")
 */
class ProductWomenController extends AbstractController
{
    private const PER_PAGE = 8;

    /**
     * @Route("/categories", name="categories")
     * @param CategoryFetcher $fetcher
     * @return Response
     */
    public function women(CategoryFetcher $fetcher): Response
    {
        $categories = $fetcher->forSex(Product::SEX_FEMALE);

        return $this->render('app/shop/shop/categories.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/category/{id}", name="category")
     * @param Request $request
     * @param Category $category
     * @param ProductFetcher $products
     * @param SubcategoryFetcher $subcategory
     * @return Response
     */
    public function shopCategories(Request $request, Category $category, ProductFetcher $products, SubcategoryFetcher $subcategory): Response
    {
        $filter = new Filter($category->getId());
        $filter->sex = Product::SEX_FEMALE;

        $subcategories = $subcategory->getAvailableSubcategory($category->getId());

        $form = $this->createForm(Form::class, $filter, array('category' => $category->getId()));
        $form->handleRequest($request);

        $pagination = $products->ForSex(
            $filter,
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'name'),
            $request->query->get('direction', 'desc')
        );

        return $this->render('app/shop/shop/show.html.twig', [
            'pagination' => $pagination,
            'subcategories' => $subcategories,
            'category' => $category,
            'cart' => $request->getSession()->get('cart'),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/product/{id}", name="product")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function showProduct(Request $request, Product $product): Response
    {
        return $this->render('app/shop/products/show.html.twig', [
            'product' => $product,
            'cart' => $request->getSession()->get('cart'),
        ]);
    }
}