<?php

declare(strict_types=1);

namespace App\Controller\Shop;

use App\Controller\ErrorHandler;
use App\Model\Product\Entity\Product\ProductRepository;
use App\Model\Product\UseCase\Cart\Show\Handler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/shop/cart", name="shop.cart.")
 */
class ShoppingCartController extends AbstractController
{
    private $errors;

    /**
     * UsersController constructor.
     * @param ErrorHandler $errors
     */
    public function __construct(ErrorHandler $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @Route("/show", name="show")
     * @param Handler $handler
     * @return Response
     */
    public function index(Handler $handler): Response
    {
        $session = new Session();
        if ($session->get('cart') == null) {
            return $this->render('app/shop/showCart.html.twig', [
                'cart' => null,
            ]);
        }

        try {
            $productsInSession = $session->get('cart');
            $cart = $handler->handle($productsInSession);
            return $this->render('app/shop/showCart.html.twig', [
                'cart' => $cart,
            ]);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
            return $this->redirectToRoute('shop.men.categories');
        }
    }

    /**
     * @Route("/remove", name="remove")
     * @param Request $request
     * @return JsonResponse
     */
    public function removeProduct(Request $request): JsonResponse
    {
        if ($request->isXmlHttpRequest()) {
            $data = $request->getContent();
            $session = $request->getSession();
            $products = $session->get('cart');
            if (array_search($data, $products) === false) {
                return new JsonResponse('Not found this product in your cart');
            }
            array_splice($products, array_search($data, $products), 1);
            $count = count($products);
            $session->set('cart', $products);
            $session->set('cartCount', $count);
            return new JsonResponse('Product removed from your cart');
        }
        return new JsonResponse([
            'type' => 'error',
            'message' => 'Ajax only!'
        ]);
    }

    /**
     * @Route("/add", name="add")
     * @param Request $request
     * @param ProductRepository $productRepository
     * @return JsonResponse
     */
    public function addProduct(Request $request, ProductRepository $productRepository): JsonResponse
    {
        if ($request->isXmlHttpRequest()) {
            try {
                $data = $request->getContent();
                $product = $productRepository->get(intval($data));
                $session = $request->getSession();
                if ($session->get('cart') == null) {
                    $session->set('cart', array());
                }
                $products = $session->get('cart');
                $count = count($products);
                array_push($products, $product->getId());
                $session->set('cart', $products);
                $session->set('cartCount', $count + 1);
                return new JsonResponse('Product added in your cart');
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                return new JsonResponse([
                    'type' => 'error',
                    'message' => $e->getMessage(),
                ]);
            }
        }

        return new JsonResponse([
            'type' => 'error',
            'message' => 'Ajax only!'
        ]);
    }
}