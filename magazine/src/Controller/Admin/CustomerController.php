<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\Product\Entity\Customer\Customer;
use App\ReadModel\Product\CustomerFetcher;
use App\ReadModel\Product\OrderFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin")
 * @IsGranted("ROLE_ADMIN")
 */
class CustomerController extends AbstractController
{
    private const PER_PAGE = 10;

    /**
     * @Route("/customers", name=".customers")
     * @param Request $request
     * @param CustomerFetcher $fetcher
     * @return Response
     */
    public function index(Request $request, CustomerFetcher $fetcher): Response
    {
        $pagination = $fetcher->all(
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'name'),
            $request->query->get('direction', 'desc')
        );

        return $this->render('admin/customer/customers.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/customers/{id}", name=".customers.show")
     * @param Customer $customer
     * @param OrderFetcher $fetcher
     * @return Response
     */
    public function show(Customer $customer, OrderFetcher $fetcher): Response
    {
        $orders = $fetcher->forCustomer($customer->getId()->getValue());
        return $this->render('admin/customer/show.html.twig', [
            'customer' => $customer,
            'orders' => $orders ? $orders : null
        ]);
    }
}