<?php


namespace App\Controller\Admin;


use App\Controller\ErrorHandler;
use App\Model\Product\Entity\Product\Brand;
use App\Model\Product\Entity\Product\Image;
use App\ReadModel\Product\BrandFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\Product\UseCase\Brand\Create;
use App\Model\Product\UseCase\Brand\Edit;
use App\Model\Product\UseCase\Brand\Delete;
use App\Model\Product\UseCase\Brand\AttachImage;
use App\Model\Product\UseCase\DeleteImage;

/**
 * @Route("/admin", name="admin")
 * @IsGranted("ROLE_ADMIN")
 */
class BrandController extends AbstractController
{
    private $errors;

    /**
     * UsersController constructor.
     * @param ErrorHandler $errors
     */
    public function __construct(ErrorHandler $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @Route("/brand/list", name=".brands")
     * @param BrandFetcher $fetcher
     * @return Response
     */
    public function index(BrandFetcher $fetcher): Response
    {
        $brands = $fetcher->all();
        return $this->render('admin/brand/brands.html.twig', compact('brands'));
    }

    /**
     * @Route("/brand/{id}", name=".brands.show")
     * @param Brand $brand
     * @return Response
     */
    public function show(Brand $brand): Response
    {
        return $this->render('admin/brand/show.html.twig', compact('brand'));
    }

    /**
     * @Route("/brand/create", name=".brands.create")
     * @param Request $request
     * @param Create\Handler $handler
     * @return Response
     */
    public function create(Request $request, Create\Handler $handler): Response
    {
        $command = new Create\Command();

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('admin.brands');
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/brand/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/brand/{id}/edit", name=".brands.edit")
     * @param Brand $brand
     * @param Request $request
     * @param Edit\Handler $handler
     * @return Response
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function edit(Brand $brand, Request $request, Edit\Handler $handler): Response
    {
        $command = Edit\Command::fromBrand($brand);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('admin.brands.show', ['id' => $brand->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/brand/edit.html.twig', [
            'brand' => $brand,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/brand/{id}/delete", name=".brands.delete", methods={"POST"})
     * @param Brand $brand
     * @param Request $request
     * @param Delete\Handler $handler
     * @return Response
     */
    public function delete(Brand $brand, Request $request, Delete\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.brands');
        }

        $command = new Delete\Command($brand->getId());

        try {
            $handler->handle($command);
            return $this->redirectToRoute('admin.brands');
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.brands');
    }

    /**
     * @Route("/brand/image/{id}/attach", name=".brands.image.attach")
     * @param Brand $brand
     * @param Request $request
     * @param AttachImage\Handler $handler
     * @return Response
     */
    public function attachImage(Brand $brand, Request $request, AttachImage\Handler $handler): Response
    {
        $command = new AttachImage\Command($brand->getId());
        $form = $this->createForm(AttachImage\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $command->parameter = $this->getParameter('product_brand_images_directory');
                $handler->handle($command);
                return $this->redirectToRoute('admin.brands.show', ['id' => $brand->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/brand/attachImage.html.twig', [
            'brand' => $brand,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/brand/image/{id}/delete", name=".brands.image.delete", methods={"POST"})
     * @param Image $image
     * @param Request $request
     * @param DeleteImage\Handler $handler
     * @return Response
     */
    public function deleteImage(Image $image, Request $request, DeleteImage\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('deleteImage', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.brands.show', ['id' => $image->getBrand()->getId()]);
        }

        $command = new DeleteImage\Command($image->getId());

        try {
            $handler->handle($command);
            return $this->redirectToRoute('admin.brands.show', ['id' => $image->getBrand()->getId()]);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.brands.show', ['id' => $image->getBrand()->getId()]);
    }
}