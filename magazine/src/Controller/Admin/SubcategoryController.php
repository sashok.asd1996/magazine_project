<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\Product\Entity\Product\Category;
use App\Model\Product\Entity\Product\Image;
use App\Model\Product\Entity\Product\Subcategory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Controller\ErrorHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\Product\UseCase\Subcategory\Delete;
use App\Model\Product\UseCase\Subcategory\Create;
use App\Model\Product\UseCase\Subcategory\Edit;
use App\Model\Product\UseCase\Subcategory\AttachImage;
use App\Model\Product\UseCase\DeleteImage;

/**
 * @Route("/admin", name="admin")
 * @IsGranted("ROLE_ADMIN")
 */
class SubcategoryController extends AbstractController
{
    private $errors;

    /**
     * UsersController constructor.
     * @param ErrorHandler $errors
     */
    public function __construct(ErrorHandler $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @Route("/subcategories/{id}", name=".subcategories.show")
     * @param Subcategory $subcategory
     * @return Response
     */
    public function show(Subcategory $subcategory): Response
    {
        return $this->render('admin/category/subcategory/show.html.twig', compact('subcategory'));
    }

    /**
     * @Route("/subcategories/{id}/create", name=".subcategories.create")
     * @param Category $category
     * @param Request $request
     * @param Create\Handler $handler
     * @return Response
     */
    public function create(Category $category, Request $request, Create\Handler $handler): Response
    {
        $command = new Create\Command($category->getId());

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $command->parameter = $this->getParameter('product_subcategory_images_directory');
                $handler->handle($command);
                return $this->redirectToRoute('admin.categories.show', ['id' => $category->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/category/subcategory/create.html.twig', [
            'form' => $form->createView(),
            'category' => $category,
        ]);
    }

    /**
     * @Route("/subcategories/{id}/delete", name=".subcategories.delete", methods={"POST"})
     * @param Subcategory $subcategory
     * @param Request $request
     * @param Delete\Handler $handler
     * @return Response
     */
    public function delete(Subcategory $subcategory, Request $request, Delete\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.categories.show', ['id' => $subcategory->getCategory()->getId()]);
        }

        $command = new Delete\Command($subcategory->getId());

        try {
            $handler->handle($command);
            return $this->redirectToRoute('admin.categories.show', ['id' => $subcategory->getCategory()->getId()]);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.categories.show', ['id' => $subcategory->getCategory()->getId()]);
    }

    /**
     * @Route("/subcategories/{id}/edit", name=".subcategories.edit")
     * @param Subcategory $subcategory
     * @param Request $request
     * @param Edit\Handler $handler
     * @return Response
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function edit(Subcategory $subcategory, Request $request, Edit\Handler $handler): Response
    {
        $command = Edit\Command::fromSubcategory($subcategory);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('admin.subcategories.show', ['id' => $subcategory->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/category/subcategory/edit.html.twig', [
            'subcategory' => $subcategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/subcategories/image/{id}/attach", name=".subcategories.image.attach")
     * @param Subcategory $subcategory
     * @param Request $request
     * @param AttachImage\Handler $handler
     * @return Response
     */
    public function attachImage(Subcategory $subcategory, Request $request, AttachImage\Handler $handler): Response
    {
        $command = new AttachImage\Command($subcategory->getId());
        $form = $this->createForm(AttachImage\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $command->parameter = $this->getParameter('product_subcategory_images_directory');
                $handler->handle($command);
                return $this->redirectToRoute('admin.subcategories.show', ['id' => $subcategory->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/category/subcategory/attachImage.html.twig', [
            'subcategory' => $subcategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/subcategories/image/{id}/delete", name=".subcategories.image.delete", methods={"POST"})
     * @param Image $image
     * @param Request $request
     * @param DeleteImage\Handler $handler
     * @return Response
     */
    public function deleteImage(Image $image, Request $request, DeleteImage\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('deleteImage', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.subcategories.show', ['id' => $image->getSubcategory()->getId()]);
        }

        $command = new DeleteImage\Command($image->getId());

        try {
            $handler->handle($command);
            return $this->redirectToRoute('admin.subcategories.show', ['id' => $image->getSubcategory()->getId()]);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.subcategories.show', ['id' => $image->getSubcategory()->getId()]);
    }
}