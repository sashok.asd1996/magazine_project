<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\ErrorHandler;
use App\Model\User\Entity\User\User;
use App\Model\User\UseCase\Create;
use App\Model\User\UseCase\Edit;
use App\Model\User\UseCase\Role;
use App\Model\User\UseCase\SignUp\Confirm\Manual\Command;
use App\Model\User\UseCase\SignUp\Confirm\Manual\Handler;
use App\ReadModel\User\Filter\Filter;
use App\ReadModel\User\Filter\Form;
use App\ReadModel\User\UserFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin")
 * @IsGranted("ROLE_ADMIN")
 */
class UsersController extends AbstractController
{
    private const PER_PAGE = 2;

    private $errors;

    /**
     * UsersController constructor.
     * @param ErrorHandler $errors
     */
    public function __construct(ErrorHandler $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @Route("", name=".users")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, UserFetcher $fetcher): Response
    {
        $filter = new Filter();

        $form = $this->createForm(Form::class, $filter);
        $form->handleRequest($request);

        $pagination = $fetcher->all(
            $filter,
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'date'),
            $request->query->get('direction', 'desc')
        );

        return $this->render('admin/user/users.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/users/create", name=".users.create")
     * @param Request $request
     * @param Create\Handler $handler
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function create(Request $request, Create\Handler $handler): Response
    {
        $command = new Create\Command();

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('admin.users');
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/user/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/users/{id}/edit", name=".users.edit")
     * @param Request $request
     * @param Edit\Handler $handler
     * @param User $user
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function edit(User $user, Request $request, Edit\Handler $handler): Response
    {
        if ($user->getId()->getValue() === $this->getUser()->getId()) {
            $this->addFlash('error', 'Unable to edit yourself.');
            return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
        }
        $command = Edit\Command::fromUser($user);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/users/{id}/role", name=".users.role")
     * @param Request $request
     * @param Role\Handler $handler
     * @param User $user
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function role(User $user, Request $request, Role\Handler $handler): Response
    {
        if ($user->getId()->getValue() === $this->getUser()->getId()) {
            $this->addFlash('error', 'Unable to change role for yourself.');
            return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
        }

        $command = Role\Command::fromUser($user);

        $form = $this->createForm(Role\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/user/role.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/users/{id}/confirm", name=".users.confirm", methods={"POST"})
     * @param User $user
     * @param Request $request
     * @param Handler $handler
     * @return Response
     */
    public function confirm(User $user, Request $request, Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('confirm', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
        }

        $command = new Command($user->getId()->getValue());

        try {
            $handler->handle($command);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
    }

    /**
     * @Route("/users/{id}/block", name=".users.block", methods={"POST"})
     * @param User $user
     * @param Request $request
     * @param \App\Model\User\UseCase\Block\Handler $handler
     * @return Response
     */
    public function block(User $user, Request $request, \App\Model\User\UseCase\Block\Handler $handler): Response
    {
        if ($user->getId()->getValue() === $this->getUser()->getId()) {
            $this->addFlash('error', 'Unable to block yourself.');
            return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
        }

        if (!$this->isCsrfTokenValid('block', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
        }

        $command = new \App\Model\User\UseCase\Block\Command($user->getId()->getValue());

        try {
            $handler->handle($command);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
    }

    /**
     * @Route("/users/{id}/activate", name=".users.activate", methods={"POST"})
     * @param User $user
     * @param Request $request
     * @param \App\Model\User\UseCase\Activate\Handler $handler
     * @return Response
     */
    public function activate(User $user, Request $request, \App\Model\User\UseCase\Activate\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('activate', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
        }

        $command = new \App\Model\User\UseCase\Activate\Command($user->getId()->getValue());

        try {
            $handler->handle($command);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.users.show', ['id' => $user->getId()]);
    }

    /**
     * @Route("/users/{id}", name=".users.show")
     * @param User $user
     * @return Response
     */
    public function show(User $user): Response
    {
        return $this->render('admin/user/show.html.twig', compact('user'));
    }
}