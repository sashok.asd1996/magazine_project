<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\ErrorHandler;
use App\Model\Product\Entity\Product\Image;
use App\Model\Product\Entity\Product\Product;
use App\ReadModel\Product\Filter\Filter;
use App\ReadModel\Product\Filter\Form;
use App\ReadModel\Product\ProductFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Product\UseCase\Product\Create;
use App\Model\Product\UseCase\DeleteImage;
use App\Model\Product\UseCase\Product\Edit;
use App\Model\Product\UseCase\Product\Remove;
use App\Model\Product\UseCase\Product\AttachImage;

/**
 * @Route("/admin", name="admin")
 * @IsGranted("ROLE_ADMIN")
 */
class ProductController extends AbstractController
{
    private const PER_PAGE = 10;

    private $errors;

    /**
     * UsersController constructor.
     * @param ErrorHandler $errors
     */
    public function __construct(ErrorHandler $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @Route("/products", name=".products")
     * @param Request $request
     * @param ProductFetcher $fetcher
     * @return Response
     */
    public function index(Request $request, ProductFetcher $fetcher): Response
    {
        $filter = new Filter();

        $form = $this->createForm(Form::class, $filter);
        $form->handleRequest($request);

        $pagination = $fetcher->all(
            $filter,
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'name'),
            $request->query->get('direction', 'desc')
        );

        return $this->render('admin/product/products.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/products/create", name=".products.create")
     * @param Request $request
     * @param Create\Handler $handler
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function create(Request $request, Create\Handler $handler): Response
    {
        $command = new Create\Command();

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $command->parameter = $this->getParameter('product_images_directory');
                $handler->handle($command);
                return $this->redirectToRoute('admin.products');
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/product/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/products/{id}", name=".products.show")
     * @param Product $product
     * @return Response
     */
    public function show(Product $product): Response
    {
        return $this->render('admin/product/show.html.twig', compact('product'));
    }

    /**
     * @Route("/products/image/{id}/attach", name=".products.image.attach")
     * @param Product $product
     * @param Request $request
     * @param AttachImage\Handler $handler
     * @return Response
     */
    public function attachImage(Product $product, Request $request, AttachImage\Handler $handler): Response
    {
        $command = new AttachImage\Command($product->getId());
        $form = $this->createForm(AttachImage\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $command->parameter = $this->getParameter('product_images_directory');
                $handler->handle($command);
                return $this->redirectToRoute('admin.products.show', ['id' => $product->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/product/attachImage.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/products/image/{id}/delete", name=".products.image.delete", methods={"POST"})
     * @param Image $image
     * @param Request $request
     * @param DeleteImage\Handler $handler
     * @return Response
     */
    public function deleteImage(Image $image, Request $request, DeleteImage\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('deleteImage', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.products.show', ['id' => $image->getProduct()->getId()]);
        }

        $command = new DeleteImage\Command($image->getId());

        try {
            $handler->handle($command);
            return $this->redirectToRoute('admin.products.show', ['id' => $image->getProduct()->getId()]);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.products.show', ['id' => $image->getProduct()->getId()]);
    }

    /**
     * @Route("/products/{id}/edit", name=".products.edit")
     * @param Request $request
     * @param Edit\Handler $handler
     * @param Product $product
     * @return Response
     */
    public function edit(Product $product, Request $request, Edit\Handler $handler): Response
    {
        $command = Edit\Command::fromProduct($product);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('admin.products.show', ['id' => $product->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/products/{id}/delete", name=".products.delete")
     * @param Request $request
     * @param Remove\Handler $handler
     * @param Product $product
     * @return Response
     */
    public function delete(Product $product, Request $request, Remove\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.products.show', ['id' => $product->getId()]);
        }

        $command = new Remove\Command($product->getId());

        try {
            $handler->handle($command);
            return $this->redirectToRoute('admin.products');
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.products.show', ['id' => $product->getId()]);
    }
}