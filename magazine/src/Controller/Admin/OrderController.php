<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\Product\Entity\Order\Order;
use App\ReadModel\Product\OrderFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin")
 * @IsGranted("ROLE_ADMIN")
 */
class OrderController extends AbstractController
{
    private const PER_PAGE = 10;

    /**
     * @Route("/orders", name=".orders")
     * @param Request $request
     * @param OrderFetcher $fetcher
     * @return Response
     */
    public function index(Request $request, OrderFetcher $fetcher): Response
    {
        $pagination = $fetcher->all(
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'created'),
            $request->query->get('direction', 'desc')
        );

        return $this->render('admin/order/orders.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/orders/{id}", name=".orders.show")
     * @param Order $order
     * @return Response
     */
    public function show(Order $order): Response
    {
        return $this->render('admin/order/show.html.twig', compact('order'));
    }
}