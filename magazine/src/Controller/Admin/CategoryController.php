<?php


namespace App\Controller\Admin;

use App\Controller\ErrorHandler;
use App\Model\Product\Entity\Product\Category;
use App\Model\Product\Entity\Product\Image;
use App\ReadModel\Product\CategoryFetcher;
use App\ReadModel\Product\SubcategoryFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Product\UseCase\Category\Create;
use App\Model\Product\UseCase\Category\Edit;
use App\Model\Product\UseCase\Category\Delete;
use App\Model\Product\UseCase\Category\AttachImage;
use App\Model\Product\UseCase\DeleteImage;

/**
 * @Route("/admin", name="admin")
 * @IsGranted("ROLE_ADMIN")
 */
class CategoryController extends AbstractController
{
    private $errors;

    /**
     * UsersController constructor.
     * @param ErrorHandler $errors
     */
    public function __construct(ErrorHandler $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @Route("/categories", name=".categories")
     * @param CategoryFetcher $fetcher
     * @return Response
     */
    public function index(CategoryFetcher $fetcher): Response
    {
        $categories = $fetcher->all();
        return $this->render('admin/category/categories.html.twig', compact('categories'));
    }

    /**
     * @Route("/categories/create", name=".categories.create")
     * @param Request $request
     * @param Create\Handler $handler
     * @return Response
     */
    public function create(Request $request, Create\Handler $handler): Response
    {
        $command = new Create\Command();

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $command->parameter = $this->getParameter('product_category_images_directory');
                $handler->handle($command);
                return $this->redirectToRoute('admin.categories');
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/category/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/categories/{id}/edit", name=".categories.edit")
     * @param Category $category
     * @param Request $request
     * @param Edit\Handler $handler
     * @return Response
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function edit(Category $category, Request $request, Edit\Handler $handler): Response
    {
        $command = Edit\Command::fromCategory($category);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('admin.categories.show', ['id' => $category->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/categories/image/{id}/attach", name=".categories.image.attach")
     * @param Category $category
     * @param Request $request
     * @param AttachImage\Handler $handler
     * @return Response
     */
    public function attachImage(Category $category, Request $request, AttachImage\Handler $handler): Response
    {
        $command = new AttachImage\Command($category->getId());
        $form = $this->createForm(AttachImage\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $command->parameter = $this->getParameter('product_category_images_directory');
                $handler->handle($command);
                return $this->redirectToRoute('admin.categories.show', ['id' => $category->getId()]);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/category/attachImage.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/categories/image/{id}/delete", name=".categories.image.delete", methods={"POST"})
     * @param Image $image
     * @param Request $request
     * @param DeleteImage\Handler $handler
     * @return Response
     */
    public function deleteImage(Image $image, Request $request, DeleteImage\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('deleteImage', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.categories.show', ['id' => $image->getCategory()->getId()]);
        }

        $command = new DeleteImage\Command($image->getId());

        try {
            $handler->handle($command);
            return $this->redirectToRoute('admin.categories.show', ['id' => $image->getCategory()->getId()]);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.categories.show', ['id' => $image->getCategory()->getId()]);
    }

    /**
     * @Route("/categories/{id}/delete", name=".categories.delete", methods={"POST"})
     * @param Category $category
     * @param Request $request
     * @param Delete\Handler $handler
     * @return Response
     */
    public function delete(Category $category, Request $request, Delete\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('admin.categories');
        }

        $command = new Delete\Command($category->getId());

        try {
            $handler->handle($command);
            return $this->redirectToRoute('admin.categories');
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin.categories');
    }

    /**
     * @Route("/categories/{id}", name=".categories.show")
     * @param Category $category
     * @param SubcategoryFetcher $fetcher
     * @return Response
     */
    public function show(Category $category, SubcategoryFetcher $fetcher): Response
    {
        $products = $fetcher->countProductsInSub($category->getId());
        return $this->render('admin/category/show.html.twig', compact('category', 'products'));
    }
}