<?php

declare(strict_types=1);

namespace App\Controller\Chat;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/chat", name="chat.")
 */
class ChatController extends AbstractController
{
    /**
     * @Route("/add", name="add")
     * @param Request $request
     * @return JsonResponse
     */
    public function add(Request $request): JsonResponse
    {
        if ($request->isXmlHttpRequest()) {
            $session = $request->getSession();
            if ($session->get('chat') == null) {
                $session->set('chat', array());
            }
            $data = $request->getContent();
            $data = json_decode($data);
            if ($this->isGranted('ROLE_ADMIN')) {
                if ($session->get('usersChat') == null) {
                    $session->set('usersChat', array());
                }
                $users = $session->get('usersChat');
                if ($data->to == 'admin') {
                    array_push($users, $data->from);
                } else {
                    array_push($users, $data->to);
                }
                $session->set('usersChat', array_unique($users));
            }
            if ($data->to == $session->getId() || $data->from == $session->getId() || $this->isGranted('ROLE_ADMIN'))
            {
                $chat = $session->get('chat');
                array_push($chat, $data);
                $session->set('chat', $chat);
                return new JsonResponse('Refresh');
            }
            return new JsonResponse('Not for you');
        }

        return new JsonResponse([
            'type' => 'error',
            'message' => 'Ajax only!'
        ]);
    }

    /**
     * @Route("/user", name="user")
     * @param Request $request
     * @return JsonResponse
     */
    public function chatUser(Request $request): JsonResponse
    {
        if ($request->isXmlHttpRequest()) {
            $session = $request->getSession();
            $data = $request->getContent();
            $data = json_decode($data);
            $chats = $session->get('chat');
            foreach ($chats as $key => $chat) {
                if (!($data->user_id == $chat->from || $data->user_id == $chat->to))
                {
                    unset($chats[$key]);
                }
            }
            return new JsonResponse($chats);
        }

        return new JsonResponse([
            'type' => 'error',
            'message' => 'Ajax only!'
        ]);
    }
}