<?php

declare(strict_types=1);

namespace App\Controller\Auth\OAuth\Facebook;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FacebookController extends AbstractController
{
    /**
     * @Route("/login/facebook", name="oauth.facebook")
     * @param ClientRegistry $clientRegistry
     * @return Response
     */
    public function connectAction(ClientRegistry $clientRegistry): Response
    {
        return $clientRegistry
            ->getClient('facebook_main')
            ->redirect(['public_profile']);
    }

    /**
     * @Route("/oauth/facebook/check", name="oauth.facebook_check")
     * @return Response
     */
    public function connectCheckAction(): Response
    {
        return $this->redirectToRoute('home');
    }
}