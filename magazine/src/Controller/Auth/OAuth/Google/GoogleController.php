<?php

declare(strict_types=1);

namespace App\Controller\Auth\OAuth\Google;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GoogleController extends AbstractController
{
    /**
     * @Route("/login/google", name="oauth.google")
     * @param ClientRegistry $clientRegistry
     * @return Response
     */
    public function connectAction(ClientRegistry $clientRegistry): Response
    {
        return $clientRegistry
            ->getClient('google')
            ->redirect(['https://www.googleapis.com/auth/userinfo.profile']);
    }

    /**
     * @Route("/oauth/google/check", name="oauth.google_check")
     * @return Response
     */
    public function connectCheckAction(): Response
    {
        return $this->redirectToRoute('home');
    }
}