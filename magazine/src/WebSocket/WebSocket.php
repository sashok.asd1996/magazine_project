<?php

declare(strict_types=1);

namespace App\WebSocket;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class WebSocket implements MessageComponentInterface
{
    protected $clients = array();
    protected $connections = array();

    function onOpen(ConnectionInterface $conn)
    {
        $this->connections[] = $conn;
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $data = json_decode($msg);

        if (property_exists($data, 'userId')) {
            $this->clients[$data->userId] = $from;
        }
        if (property_exists($data, 'adminId')) {
            $this->clients['admin'] = $from;
        }

        if (property_exists($data, 'type') and $data->type == 'shop')
        {
            foreach ($this->clients as $client) {
                if ($from !== $client) {
                    $client->send($msg);
                }
            }
        }
        if (property_exists($data, 'type') and $data->type == 'chatAdmin')
        {
            if (property_exists($data, 'who') and $data->who == 'admin') {
                if (array_key_exists($data->toUserId, $this->clients)) {
                    $admin = $this->clients['admin'];
                    $admin->send($msg);
                    $this->clients[$data->toUserId]->send($msg);
                }
                else {
                    $data->text = 'User offline';
                    $from->send(json_encode($data));
                }
            } else {
                if (array_key_exists('admin', $this->clients)) {
                    $from->send($msg);
                    $admin = $this->clients['admin'];
                    $admin->send($msg);
                }
                else {
                    $data->text = 'Admin offline';
                    $from->send(json_encode($data));
                }
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $id = array_search($conn, $this->clients);
        unset($this->clients[$id]);
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

}