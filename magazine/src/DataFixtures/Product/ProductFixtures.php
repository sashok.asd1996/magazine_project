<?php

declare(strict_types=1);

namespace App\DataFixtures\Product;

use App\Model\Product\Entity\Product\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $categoryArray = array();
        $i = 0;
        while ($this->hasReference('category_' . $i)) {
            $categoryArray[] = $this->getReference('category_' . $i);
            ++$i;
        }

        $brandArray = array();
        $i = 0;
        while (!empty($this->hasReference('brand_' . $i))) {
            $brandArray[] = $this->getReference('brand_' . $i);
            ++$i;
        }

        for ($i = 0; $i < 10000; $i++) {
            $brand = $faker->randomElement($brandArray);
            $category = $faker->randomElement($categoryArray);
            $product = new Product(
                $faker->sentence(random_int(1,4), $variableNbWords = true),
                $faker->randomFloat(2, 1, 10000),
                $category->getSex(),
                $faker->randomElement([
                    Product::COLOR_YELLOW,
                    Product::COLOR_WHITE,
                    Product::COLOR_RED,
                    Product::COLOR_PURPLE,
                    Product::COLOR_BLUE,
                    Product::COLOR_BLACK
                ]),
                $faker->randomElement([
                    Product::SIZE_M,
                    Product::SIZE_S
                ]),
                $faker->randomElement([
                    Product::SEASON_WINTER,
                    Product::SEASON_SUMMER
                ]),
                $faker->randomElement($category->getSubcategories()),
                $brand,
                $faker->paragraph($nbSentences = 3, $variableNbSentences = true)
            );

            $manager->persist($product);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            CategoryFixtures::class,
            BrandFixture::class,
        ];
    }
}