<?php

declare(strict_types=1);

namespace App\DataFixtures\Product;

use App\Model\Product\Entity\Product\Category;
use App\Model\Product\Entity\Product\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $categoryArray = array(
            'Clothing',
            'Shoes',
            'Accessories',
            'Activewear',
            'Dresses',
            'Tops',
            'Swimwear',
            'Jumpsuits & Rompers',
        );

        $subCategoryClothing = array(
            'T-shirts & Tanks',
            'Shorts & Swimwear',
            'Tailoring',
            'Jeans',
            'Shirts',
            'Pants & Chinos',
            'Hoodies & Sweatshirts',
            'Jackets & Coats'
        );

        $subCategoryShoes = array(
            'Boots',
            'Shoes',
            'Sliders'
        );

        $subCategoryAccessories = array(
            'Bags',
            'Belts',
            'Caps & Hats'
        );

        $subCategoryActivewear = array(
            'Jackets',
            'Pants & Tights',
            'Swim'
        );

        $subCategoryDresses = array(
            'Casual Dresses',
            'Party Dresses',
            'Midi Dresses'
        );

        $subCategoryTops = array(
            'T-shirts & Tanks',
            'Kimonos',
            'Crop Tops'
        );

        $subCategorySwimwear = array(
            'Bikinis',
            'Beach Clothing',
            'Swimsuits'
        );

        $subCategoryJumpsuits = array(
            'Jumpsuits & Rompers'
        );

        for ($i = 0; $i < count($categoryArray); $i++){
            if ($i <= 3) {
                $category = new Category($categoryArray[$i], Product::SEX_MALE);
            }
            else {
                $category = new Category($categoryArray[$i], Product::SEX_FEMALE);
            }
            if ($i == 0) {
                for ($j = 0; $j < count($subCategoryClothing); $j++){
                    $category->setSubcategories($subCategoryClothing[$j]);
                }
            }
            if ($i == 1) {
                for ($j = 0; $j < count($subCategoryShoes); $j++){
                    $category->setSubcategories($subCategoryShoes[$j]);
                }
            }
            if ($i == 2) {
                for ($j = 0; $j < count($subCategoryAccessories); $j++){
                    $category->setSubcategories($subCategoryAccessories[$j]);
                }
            }
            if ($i == 3) {
                for ($j = 0; $j < count($subCategoryActivewear); $j++){
                    $category->setSubcategories($subCategoryActivewear[$j]);
                }
            }
            if ($i == 4) {
                for ($j = 0; $j < count($subCategoryDresses); $j++){
                    $category->setSubcategories($subCategoryDresses[$j]);
                }
            }
            if ($i == 5) {
                for ($j = 0; $j < count($subCategoryTops); $j++){
                    $category->setSubcategories($subCategoryTops[$j]);
                }
            }
            if ($i == 6) {
                for ($j = 0; $j < count($subCategorySwimwear); $j++){
                    $category->setSubcategories($subCategorySwimwear[$j]);
                }
            }
            if ($i == 7) {
                for ($j = 0; $j < count($subCategoryJumpsuits); $j++){
                    $category->setSubcategories($subCategoryJumpsuits[$j]);
                }
            }
            $manager->persist($category);
            $this->setReference('category_' . $i, $category);
        }
        $manager->flush();
    }

}