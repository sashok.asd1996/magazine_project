<?php

declare(strict_types=1);

namespace App\DataFixtures\Product;

use App\Model\Product\Entity\Product\Brand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BrandFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $brandArray = array(
            'Armani Exchange',
            'BOSS',
            'Calvin Klein',
            'Diesel',
            'G-Star',
            'Jack & Jones',
            'Lacoste',
            'Levi\'s',
            'Lockstock',
            'Lyle & Scott',
            'MOSS BROS',
            'New Era',
            'Pull&Bear',
            'Rip N Dip',
            'Tommy Hilfiger',
            'Vintage Supply',
        );

        for($i = 0; $i < count($brandArray); $i++){
            $brand = new Brand($brandArray[$i]);
            $manager->persist($brand);
            $this->setReference('brand_' . $i, $brand);
        }
        $manager->flush();
    }

}