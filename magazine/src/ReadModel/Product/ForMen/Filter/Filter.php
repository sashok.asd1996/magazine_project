<?php

declare(strict_types=1);

namespace App\ReadModel\Product\ForMen\Filter;


use App\Model\Product\Entity\Product\Product;

class Filter
{
    public $name;
    public $price;
    public $sex = Product::SEX_MALE;
    public $color;
    public $size;
    public $season;
    public $category;
    public $subcategory;
    public $brand;

    /**
     * Filter constructor.
     * @param $category
     */
    public function __construct(int $category)
    {
        $this->category = $category;
    }


    /**
     * @return int
     */
    public function getCategory(): ?int
    {
        return $this->category;
    }
}