<?php

declare(strict_types=1);

namespace App\ReadModel\Product\ForMen\Filter;

use App\Model\Product\Entity\Product\Product;
use App\ReadModel\Product\BrandFetcher;
use App\ReadModel\Product\CategoryFetcher;
use App\ReadModel\Product\SubcategoryFetcher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

class Form extends AbstractType
{
    private $category;
    private $subcategory;
    private $brand;

    public function __construct(CategoryFetcher $category, BrandFetcher $brand, SubcategoryFetcher $subcategory)
    {
        $this->category = $category;
        $this->subcategory = $subcategory;
        $this->brand = $brand;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', Type\TextType::class, ['required' => false, 'attr' => [
                'placeholder' => 'Name',
                'onchange' => 'this.form.submit()',
            ]])
            ->add('price', Type\MoneyType::class, ['required' => false, 'attr' => [
                'placeholder' => 'Price',
                'onchange' => 'this.form.submit()',
            ]])
            ->add('color', Type\ChoiceType::class, ['choices' => [
                'Black' => Product::COLOR_BLACK,
                'Blue' => Product::COLOR_BLUE,
                'Purple' => Product::COLOR_PURPLE,
                'Red' => Product::COLOR_RED,
                'White' => Product::COLOR_WHITE,
                'Yellow' => Product::COLOR_YELLOW,
            ], 'required' => false, 'placeholder' => 'All colors', 'attr' => ['onchange' => 'this.form.submit()']])
            ->add('size', Type\ChoiceType::class, ['choices' => [
                'M' => Product::SIZE_M,
                'S' => Product::SIZE_S,
            ], 'required' => false, 'placeholder' => 'All size', 'attr' => ['onchange' => 'this.form.submit()']])
            ->add('season', Type\ChoiceType::class, ['choices' => [
                'Winter' => Product::SEASON_WINTER,
                'Summer' => Product::SEASON_SUMMER,
            ], 'required' => false, 'placeholder' => 'All season', 'attr' => ['onchange' => 'this.form.submit()']])
            ->add('brand', Type\ChoiceType::class, [
                'choices' => array_flip($this->brand->assoc()),
                'required' => false,
                'placeholder' => 'All brand',
                'attr' => ['onchange' => 'this.form.submit()']
            ])
            ->add('subcategory', Type\ChoiceType::class, [
                'choices' => array_flip($this->subcategory->getAvailableSubcategory($options['category'])),
                'required' => false,
                'placeholder' => 'All subcategories',
                'attr' => ['onchange' => 'this.form.submit()']
            ])
            ->add('category', Type\HiddenType::class, [
                'data' => $options['category'],
            ]);



    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filter::class,
            'category' => null,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }
}