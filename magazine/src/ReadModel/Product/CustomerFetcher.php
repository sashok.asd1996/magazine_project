<?php

declare(strict_types=1);

namespace App\ReadModel\Product;

use App\Model\Product\Entity\Customer\Customer;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class CustomerFetcher
{
    private $connection;
    private $paginator;
    private $repository;

    public function __construct(Connection $connection, PaginatorInterface $paginator, EntityManagerInterface $em)
    {
        $this->connection = $connection;
        $this->paginator = $paginator;
        $this->repository = $em->getRepository(Customer::class);
    }

    public function all(int $page, int $size, string $sort, string $direction): PaginationInterface
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'g.id',
                'u.email as user_email',
                'g.email',
                'g.mobile',
                'TRIM(CONCAT(g.name_first, \' \', g.name_last)) as name'
            )
            ->from('product_customer_customers', 'g')
            ->innerJoin('g', 'user_users', 'u', 'u.id = g.id');

        $qb->orderBy($sort, $direction === 'desc' ? 'desc' : 'asc');

        return $this->paginator->paginate($qb, $page, $size);
    }
}