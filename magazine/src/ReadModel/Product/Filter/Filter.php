<?php

declare(strict_types=1);

namespace App\ReadModel\Product\Filter;

class Filter
{
    public $name;
    public $price;
    public $sex;
    public $color;
    public $size;
    public $season;
    public $category;
    public $subcategory;
    public $brand;

    /**
     * @return int
     */
    public function getCategory(): ?int
    {
        return $this->category;
    }
}