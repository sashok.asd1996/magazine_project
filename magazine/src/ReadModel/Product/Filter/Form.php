<?php

declare(strict_types=1);

namespace App\ReadModel\Product\Filter;

use App\Model\Product\Entity\Product\Product;
use App\ReadModel\Product\BrandFetcher;
use App\ReadModel\Product\CategoryFetcher;
use App\ReadModel\Product\SubcategoryFetcher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

class Form extends AbstractType
{
    private $category;
    private $subcategory;
    private $brand;

    public function __construct(CategoryFetcher $category, BrandFetcher $brand, SubcategoryFetcher $subcategory)
    {
        $this->category = $category;
        $this->subcategory = $subcategory;
        $this->brand = $brand;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', Type\TextType::class, ['required' => false, 'attr' => [
                'placeholder' => 'Name',
                'onchange' => 'this.form.submit()',
            ]])
            ->add('price', Type\MoneyType::class, ['required' => false, 'attr' => [
                'placeholder' => 'Price',
                'onchange' => 'this.form.submit()',
            ]])
            ->add('sex', Type\ChoiceType::class, ['choices' => [
                'Male' => Product::SEX_MALE,
                'Female' => Product::SEX_FEMALE,
            ], 'required' => false, 'placeholder' => 'All sex', 'attr' => ['onchange' => 'this.form.submit()']])
            ->add('color', Type\ChoiceType::class, ['choices' => [
                'Black' => Product::COLOR_BLACK,
                'Blue' => Product::COLOR_BLUE,
                'Purple' => Product::COLOR_PURPLE,
                'Red' => Product::COLOR_RED,
                'White' => Product::COLOR_WHITE,
                'Yellow' => Product::COLOR_YELLOW,
            ], 'required' => false, 'placeholder' => 'All colors', 'attr' => ['onchange' => 'this.form.submit()']])
            ->add('size', Type\ChoiceType::class, ['choices' => [
                'M' => Product::SIZE_M,
                'S' => Product::SIZE_S,
            ], 'required' => false, 'placeholder' => 'All size', 'attr' => ['onchange' => 'this.form.submit()']])
            ->add('season', Type\ChoiceType::class, ['choices' => [
                'Winter' => Product::SEASON_WINTER,
                'Summer' => Product::SEASON_SUMMER,
            ], 'required' => false, 'placeholder' => 'All season', 'attr' => ['onchange' => 'this.form.submit()']])
            ->add('brand', Type\ChoiceType::class, [
                'choices' => array_flip($this->brand->assoc()),
                'required' => false,
                'placeholder' => 'All brand',
                'attr' => ['onchange' => 'this.form.submit()']
            ])
            ->add('category', Type\ChoiceType::class, [
                'choices' => array_flip($this->category->assoc()),
                'required' => false,
                'placeholder' => 'All categories',
                'attr' => ['onchange' => 'this.form.submit()']
            ]);

        $formModifier = function (FormInterface $form, int $category = 0) {
            $category = $category === null ? 0 : $category;
            $form->add('subcategory', Type\ChoiceType::class, [
                'choices' => array_flip($this->subcategory->getAvailableSubcategory($category)),
                'required' => false,
                'placeholder' => 'All subcategories',
                'attr' => ['onchange' => 'this.form.submit()']
            ]);
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();

                $category = $data->getCategory() === null ? 0 : $data->getCategory();

                $formModifier($event->getForm(), $category);
            }
        );

        $builder->get('category')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $category = $event->getForm()->getData() === null ? 0 : $event->getForm()->getData();

                $formModifier($event->getForm()->getParent(), $category);
            }
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filter::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }
}