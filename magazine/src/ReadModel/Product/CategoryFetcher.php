<?php

declare(strict_types=1);

namespace App\ReadModel\Product;

use App\Model\Product\Entity\Product\Image;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;

class CategoryFetcher
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function assoc(): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'id',
                'name'
            )
            ->from('product_product_category')
            ->orderBy('name')
            ->execute();

        return $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    public function all()
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'g.id',
                'g.name',
                'g.sex',
                '(SELECT COUNT(*) FROM product_products m WHERE m.subcategory_id IN (
                SELECT id FROM product_product_category_subcategory WHERE category_id = g.id)) AS products'
            )
            ->from('product_product_category', 'g')
            ->orderBy('name')
            ->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }

    public function forSex(string $sex)
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'g.id',
                'g.name',
                'g.sex',
                'i.name as images'
            )
            ->from('product_product_category', 'g')
            ->leftJoin('g', 'product_product_image', 'i', 'i.category_id = g.id')
            ->where('g.sex = :male')
            ->setParameter(':male', $sex)
            ->orderBy('g.name')
            ->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }
}