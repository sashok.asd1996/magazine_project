<?php

declare(strict_types=1);

namespace App\ReadModel\Product;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;

class SubcategoryFetcher
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function countProductsInSub(int $categoryId)
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                '(SELECT COUNT(*) FROM product_products m WHERE g.id = m.subcategory_id) AS products'
            )
            ->where('g.category_id = :categoryId')
            ->setParameter(':categoryId', $categoryId)
            ->from('product_product_category_subcategory', 'g')
            ->execute();

        return $stmt->fetchAll(FetchMode::COLUMN);
    }

    public function assoc(): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'id',
                'name'
            )
            ->from('product_product_category_subcategory')
            ->orderBy('name')
            ->execute();

        return $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    public function getAvailableSubcategory(int $id): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'id',
                'name'
            )
            ->from('product_product_category_subcategory')
            ->where('category_id = :id')
            ->setParameter(':id', $id)
            ->orderBy('name')
            ->execute();

        return $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
    }
}