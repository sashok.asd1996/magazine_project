<?php

declare(strict_types=1);

namespace App\ReadModel\Product;

use App\Model\Product\Entity\Order\Order;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class OrderFetcher
{
    private $connection;
    private $paginator;
    private $repository;

    public function __construct(Connection $connection, PaginatorInterface $paginator, EntityManagerInterface $em)
    {
        $this->connection = $connection;
        $this->paginator = $paginator;
        $this->repository = $em->getRepository(Order::class);
    }

    public function forCustomer(string $id)
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'g.id',
                'g.grand_total',
                'g.created',
                'g.status',
                '(SELECT COUNT(id) FROM product_order_order_item WHERE order_id = g.id) AS count'
            )
            ->from('product_order_orders', 'g')
            ->where('g.customer_id = :id')
            ->setParameter(':id', $id)
            ->orderBy('g.created')
            ->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }

    public function all(int $page, int $size, string $sort, string $direction): PaginationInterface
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'g.id',
                'g.customer_id',
                'TRIM(CONCAT(c.name_first, \' \', c.name_last)) as customer_name',
                'g.grand_total',
                'g.created',
                'g.status',
                '(SELECT COUNT(id) FROM product_order_order_item WHERE order_id = g.id) AS count'
            )
            ->from('product_order_orders', 'g')
            ->innerJoin('g', 'product_customer_customers', 'c', 'c.id = g.customer_id');

        $qb->orderBy($sort, $direction === 'desc' ? 'desc' : 'asc');

        return $this->paginator->paginate($qb, $page, $size);
    }
}