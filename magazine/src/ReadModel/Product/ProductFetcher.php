<?php

declare(strict_types=1);

namespace App\ReadModel\Product;

use App\Model\User\Entity\User\User;
use App\ReadModel\Product\ForMen\Filter\Filter;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class ProductFetcher
{
    private $connection;
    private $paginator;
    private $repository;

    public function __construct(Connection $connection, PaginatorInterface $paginator, EntityManagerInterface $em)
    {
        $this->connection = $connection;
        $this->paginator = $paginator;
        $this->repository = $em->getRepository(User::class);
    }

    public function all(\App\ReadModel\Product\Filter\Filter $filter, int $page, int $size, string $sort, string $direction): PaginationInterface
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'p.id',
                'p.name',
                'p.description',
                'p.price',
                'p.sex',
                'p.color',
                'p.size',
                'p.season',
                'p.subcategory_id',
                'p.brand_id',
                'c.name as category_name',
                'b.name as brand_name'
            )
            ->from('product_products', 'p')
            ->innerJoin('p', 'product_product_category_subcategory', 'c', 'p.subcategory_id = c.id')
            ->innerJoin('p', 'product_product_brand', 'b', 'p.brand_id = b.id');

        if ($filter->name) {
            $qb->andWhere($qb->expr()->like('LOWER(p.name)', ':name'));
            $qb->setParameter(':name', '%' . mb_strtolower($filter->name) . '%');
        }

        if ($filter->price) {
            $qb->andWhere($qb->expr()->like('price', ':price'));
            $qb->setParameter(':price', '%' . $filter->price . '%');
        }

        if ($filter->sex) {
            $qb->andWhere($qb->expr()->like('sex', ':sex'));
            $qb->setParameter(':sex', $filter->sex);
        }

        if ($filter->color) {
            $qb->andWhere($qb->expr()->like('color', ':color'));
            $qb->setParameter(':color', $filter->color);
        }

        if ($filter->size) {
            $qb->andWhere($qb->expr()->like('size', ':size'));
            $qb->setParameter(':size', $filter->size);
        }

        if ($filter->season) {
            $qb->andWhere($qb->expr()->like('season', ':season'));
            $qb->setParameter(':season', $filter->season);
        }

        if ($filter->category) {
            $qb->andWhere($qb->expr()->like('category_id', ':category'));
            $qb->setParameter(':category', $filter->category);
        }

        if ($filter->subcategory) {
            $qb->andWhere($qb->expr()->like('subcategory_id', ':subcategory'));
            $qb->setParameter(':subcategory', $filter->subcategory);
        }

        if ($filter->brand) {
            $qb->andWhere($qb->expr()->like('brand_id', ':brand'));
            $qb->setParameter(':brand', $filter->brand);
        }

        if (!\in_array($sort, ['name', 'price', 'sex', 'color', 'size', 'season', 'category', 'brand'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $sort);
        }

        $qb->orderBy($sort, $direction === 'desc' ? 'desc' : 'asc');

        return $this->paginator->paginate($qb, $page, $size);
    }

    public function ForSex(Filter $filter, int $page, int $size, string $sort, string $direction): PaginationInterface
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'p.id',
                'p.name',
                'p.description',
                'p.price',
                'p.color',
                'p.size',
                'p.season',
                'p.subcategory_id',
                'p.brand_id',
                'b.name as brand_name',
                's.name as subcategory_name',
                'c.name as category_name',
                'c.id as category_id',
                'i.name as image_name',
                'i.path as image_path'
            )
            ->from('product_products', 'p')
            ->innerJoin('p', 'product_product_category_subcategory', 's', 'p.subcategory_id = s.id')
            ->innerJoin('s', 'product_product_category', 'c', 's.category_id = c.id')
            ->innerJoin('p', 'product_product_brand', 'b', 'p.brand_id = b.id')
            ->leftJoin('p', 'product_product_image', 'i', 'i.product_id = p.id');

        $qb->andWhere($qb->expr()->like('s.category_id', ':category'));
        $qb->setParameter(':category', $filter->category);
        $qb->andWhere($qb->expr()->like('p.sex', ':sex'));
        $qb->setParameter(':sex', $filter->sex);

        if ($filter->name) {
            $qb->andWhere($qb->expr()->like('LOWER(p.name)', ':name'));
            $qb->setParameter(':name', '%' . mb_strtolower($filter->name) . '%');
        }

        if ($filter->price) {
            $qb->andWhere($qb->expr()->like('price', ':price'));
            $qb->setParameter(':price', '%' . $filter->price . '%');
        }

        if ($filter->color) {
            $qb->andWhere($qb->expr()->like('color', ':color'));
            $qb->setParameter(':color', $filter->color);
        }

        if ($filter->size) {
            $qb->andWhere($qb->expr()->like('size', ':size'));
            $qb->setParameter(':size', $filter->size);
        }

        if ($filter->season) {
            $qb->andWhere($qb->expr()->like('season', ':season'));
            $qb->setParameter(':season', $filter->season);
        }

        if ($filter->subcategory) {
            $qb->andWhere($qb->expr()->like('p.subcategory_id', ':subcategory'));
            $qb->setParameter(':subcategory', $filter->subcategory);
        }

        if ($filter->brand) {
            $qb->andWhere($qb->expr()->like('p.brand_id', ':brand'));
            $qb->setParameter(':brand', $filter->brand);
        }

        if (!\in_array($sort, ['name', 'price', 'sex', 'color', 'size', 'season', 'category', 'brand'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $sort);
        }

        $qb->orderBy($sort, $direction === 'desc' ? 'desc' : 'asc');

        return $this->paginator->paginate($qb, $page, $size);
    }
}