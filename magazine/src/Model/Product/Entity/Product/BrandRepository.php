<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Product;

use Doctrine\ORM\EntityManagerInterface;

class BrandRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repo = $em->getRepository(Brand::class);
        $this->em = $em;
    }

    public function get(int $id): Brand
    {
        /** @var Brand $brand */
        if (!$brand = $this->repo->find($id)) {
            throw new \DomainException('Brand is not found.');
        }
        return $brand;
    }

    public function add(Brand $brand): void
    {
        $this->em->persist($brand);
    }

    public function remove(Brand $brand): void
    {
        $this->em->remove($brand);
    }
}