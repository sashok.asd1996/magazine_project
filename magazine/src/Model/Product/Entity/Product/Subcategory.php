<?php


namespace App\Model\Product\Entity\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_product_category_subcategory")
 */
class Subcategory
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Image|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Image", mappedBy="subcategory", orphanRemoval=true, cascade={"persist"})
     */
    private $images;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="subcategories")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $category;

    /**
     * Subcategory constructor.
     * @param string $name
     * @param Category $category
     */
    public function __construct(string $name, Category $category)
    {
        $this->name = $name;
        $this->category = $category;
        $this->images = new ArrayCollection();
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Image|ArrayCollection
     */
    public function getImages()
    {
        return $this->images->toArray();
    }

    public function attachImage(string $name, string $path): void
    {
        $this->images->add(Image::forSubcategory($this, $name, $path));
    }
}