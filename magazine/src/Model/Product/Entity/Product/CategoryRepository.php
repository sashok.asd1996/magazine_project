<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Product;

use Doctrine\ORM\EntityManagerInterface;

class CategoryRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repo = $em->getRepository(Category::class);
        $this->em = $em;
    }

    public function get(int $id): Category
    {
        /** @var Category $category */
        if (!$category = $this->repo->find($id)) {
            throw new \DomainException('Group is not found.');
        }
        return $category;
    }

    public function add(Category $category): void
    {
        $this->em->persist($category);
    }

    public function remove(Category $category): void
    {
        $this->em->remove($category);
    }

}