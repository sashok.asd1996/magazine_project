<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="product_products")
 */
class Product
{
    public const SEX_MALE = 'male';
    public const SEX_FEMALE = 'female';
    public const COLOR_RED = 'red';
    public const COLOR_BLACK = 'black';
    public const COLOR_YELLOW = 'yellow';
    public const COLOR_BLUE = 'blue';
    public const COLOR_WHITE = 'white';
    public const COLOR_PURPLE = 'purple';
    public const SIZE_M = 'm';
    public const SIZE_S = 's';
    public const SEASON_WINTER = 'winter';
    public const SEASON_SUMMER = 'summer';

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10)
     */
    private $sex;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $season;

    /**
     * @var Image|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Image", mappedBy="product", orphanRemoval=true, cascade={"persist"})
     */
    private $images;

    /**
     * @var Subcategory
     *
     * @ORM\ManyToOne(targetEntity="Subcategory")
     * @ORM\JoinColumn(name="subcategory_id", referencedColumnName="id")
     */
    private $subcategory;

    /**
     * @var Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     */
    private $brand;

    /**
     * Product constructor.
     * @param string $name
     * @param string $description
     * @param float $price
     * @param string $sex
     * @param string $color
     * @param string $size
     * @param string $season
     * @param Subcategory $subcategory
     * @param Brand $brand
     */
    public function __construct(
        string $name,
        float $price,
        string $sex,
        string $color,
        string $size,
        string $season,
        Subcategory $subcategory,
        Brand $brand,
        string $description = null
    )
    {
        $this->name = $name;
        $this->price = $price;
        $this->sex = $sex;
        $this->color = $color;
        $this->size = $size;
        $this->season = $season;
        $this->subcategory = $subcategory;
        $this->brand = $brand;
        $this->description = $description;
        $this->images = new ArrayCollection();
    }

    public function edit(
        string $name,
        string $description,
        float $price,
        string $sex,
        string $color,
        string $size,
        string $season,
        Subcategory $subcategory,
        Brand $brand
    ): void
    {
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->sex = $sex;
        $this->color = $color;
        $this->size = $size;
        $this->season = $season;
        $this->subcategory = $subcategory;
        $this->brand = $brand;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return floatval($this->price);
    }

    /**
     * @return string
     */
    public function getSex(): string
    {
        return $this->sex;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @return string
     */
    public function getSeason(): string
    {
        return $this->season;
    }

    /**
     * @return Image[]
     */
    public function getImages(): array
    {
        return $this->images->toArray();
    }

    /**
     * @return Subcategory
     */
    public function getSubcategory(): Subcategory
    {
        return $this->subcategory;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @param string $sex
     */
    public function setSexMale(string $sex): void
    {
        $this->sex = self::SEX_MALE;
    }

    /**
     * @param string $sex
     */
    public function setSexFemale(string $sex): void
    {
        $this->sex = self::SEX_FEMALE;
    }

    public function attachImage(string $name, string $path): void
    {
        $this->images->add(Image::forProduct($this, $name, $path));
    }
}