<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_product_brand")
 */
class Brand
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Image|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Image", mappedBy="brand", orphanRemoval=true, cascade={"persist"})
     */
    private $images;

    /**
     * @return Image|ArrayCollection
     */
    public function getImages()
    {
        return $this->images->toArray();
    }

    /**
     * Brand constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->images = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function attachImage(string $name, string $path): void
    {
        $this->images->add(Image::forBrand($this, $name, $path));
    }
}