<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Product;

use Doctrine\ORM\EntityManagerInterface;

class ImageRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repo = $em->getRepository(Image::class);
        $this->em = $em;
    }

    public function get(int $id): Image
    {
        /** @var Category $category */
        if (!$image = $this->repo->find($id)) {
            throw new \DomainException('Image is not found.');
        }
        return $image;
    }

    public function add(Image $image): void
    {
        $this->em->persist($image);
    }

    public function remove(Image $image): void
    {
        $this->em->remove($image);
    }
}