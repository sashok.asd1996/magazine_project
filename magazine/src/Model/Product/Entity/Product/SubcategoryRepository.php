<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Product;

use Doctrine\ORM\EntityManagerInterface;

class SubcategoryRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repo = $em->getRepository(Subcategory::class);
        $this->em = $em;
    }

    public function get(int $id): Subcategory
    {
        /** @var Subcategory $subcategory */
        if (!$subcategory = $this->repo->find($id)) {
            throw new \DomainException('Subcategory is not found.');
        }
        return $subcategory;
    }

    public function add(Subcategory $subcategory): void
    {
        $this->em->persist($subcategory);
    }

    public function remove(Subcategory $subcategory): void
    {
        $this->em->remove($subcategory);
    }
}