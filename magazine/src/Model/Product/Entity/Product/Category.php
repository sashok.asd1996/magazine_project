<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_product_category")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Image|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Image", mappedBy="category", orphanRemoval=true, cascade={"persist"})
     */
    private $images;

    /**
     * @var Subcategory|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Subcategory", mappedBy="category", orphanRemoval=true, cascade={"persist"})
     */
    private $subcategories;

    /**
     * @return array
     */
    public function getSubcategories(): array
    {
        return $this->subcategories->toArray();
    }

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10)
     */
    private $sex;

    /**
     * @return Image|ArrayCollection
     */
    public function getImages()
    {
        return $this->images->toArray();
    }

    /**
     * @return string
     */
    public function getSex(): string
    {
        return $this->sex;
    }

    /**
     * Category constructor.
     * @param string $name
     * @param string $sex
     */
    public function __construct(string $name, string $sex)
    {
        $this->name = $name;
        $this->sex = $sex;
        $this->images = new ArrayCollection();
        $this->subcategories = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $sex
     */
    public function setSex(string $sex): void
    {
        $this->sex = $sex;
    }

    /**
     * @param string $name
     */
    public function setSubcategories(string $name): void
    {
        $this->subcategories->add(new Subcategory($name, $this));
    }

    public function attachImage(string $name, string $path): void
    {
        $this->images->add(Image::forCategory($this, $name, $path));
    }

}