<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Product;

use Doctrine\ORM\EntityManagerInterface;

class ProductRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;

    /**
     * UserRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(Product::class);
    }

    public function add(Product $product): void
    {
        $this->em->persist($product);
    }

    public function hasByCategory(int $id): bool
    {
        return $this->repo->createQueryBuilder('t')
                ->select('COUNT(t.id)')
                ->innerJoin(Subcategory::class, 's', 't.subcategory = s.id')
                ->where('s.category = :id')
                ->setParameter(':id', $id)
                ->getQuery()->getSingleScalarResult() > 0;
    }

    public function hasBySubcategory(int $id): bool
    {
        return $this->repo->createQueryBuilder('t')
                ->select('COUNT(t.id)')
                ->where('t.subcategory = :id')
                ->setParameter(':id', $id)
                ->getQuery()->getSingleScalarResult() > 0;
    }

    public function hasByBrand(int $id): bool
    {
        return $this->repo->createQueryBuilder('t')
                ->select('COUNT(t.id)')
                ->andWhere('t.brand = :brand')
                ->setParameter(':brand', $id)
                ->getQuery()->getSingleScalarResult() > 0;
    }

    public function get(int $id): Product
    {
        /** @var Product $product */
        if (!$product = $this->repo->find($id)) {
            throw new \DomainException('Product is not found.');
        }
        return $product;
    }

    public function remove(Product $product): void
    {
        $this->em->remove($product);
    }

}