<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_product_image")
 */
class Image
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $path;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="images")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $product;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="images")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $category;

    /**
     * @var Brand
     * @ORM\ManyToOne(targetEntity="Brand", inversedBy="images")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $brand;

    /**
     * @var Subcategory
     * @ORM\ManyToOne(targetEntity="Subcategory", inversedBy="images")
     * @ORM\JoinColumn(name="subcategory_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $subcategory;

    /**
     * Image constructor.
     * @param string $name
     * @param string $path
     */
    private function __construct(string $name, string $path)
    {
        $this->name = $name;
        $this->path = $path;
    }

    public static function forProduct(Product $product, string $name, string $path): self
    {
        $image = new self($name, $path);
        $image->product = $product;
        return $image;
    }

    public static function forCategory(Category $category, string $name, string $path): self
    {
        $image = new self($name, $path);
        $image->category = $category;
        return $image;
    }

    public static function forSubcategory(Subcategory $subcategory, string $name, string $path): self
    {
        $image = new self($name, $path);
        $image->subcategory = $subcategory;
        return $image;
    }

    public static function forBrand(Brand $brand, string $name, string $path): self
    {
        $image = new self($name, $path);
        $image->brand = $brand;
        return $image;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Subcategory
     */
    public function getSubcategory(): Subcategory
    {
        return $this->subcategory;
    }

}