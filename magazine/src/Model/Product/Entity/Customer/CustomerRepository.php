<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Customer;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class CustomerRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repo = $em->getRepository(Customer::class);
        $this->em = $em;
    }

    public function has(Id $id): bool
    {
        return $this->repo->createQueryBuilder('t')
                ->select('COUNT(t.id)')
                ->andWhere('t.id = :id')
                ->setParameter(':id', $id->getValue())
                ->getQuery()->getSingleScalarResult() > 0;
    }

    /**
     * @param Id $id
     * @return Customer
     * @throws EntityNotFoundException
     */
    public function get(Id $id): Customer
    {
        /** @var Customer $customer */
        if (!$customer = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Customer is not found.');
        }
        return $customer;
    }

    public function add(Customer $customer): void
    {
        $this->em->persist($customer);
    }
}