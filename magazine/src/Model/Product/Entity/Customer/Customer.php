<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Customer;

use App\Model\Product\Entity\Customer\Address\Address;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_customer_customers")
 */
class Customer
{
    /**
     * @var Id
     * @ORM\Column(type="customer_customer_id")
     * @ORM\Id
     */
    private $id;
    /**
     * @var Name
     * @ORM\Embedded(class="Name")
     */
    private $name;
    /**
     * @var Email
     * @ORM\Column(type="customer_customer_email", nullable=false)
     */
    private $email;
    /**
     * @var Address[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Model\Product\Entity\Customer\Address\Address", mappedBy="customer", orphanRemoval=true, cascade={"persist"})
     */
    private $addresses;

    /**
     * @var string
     * @ORM\Column(type="string", length=15, nullable=false)
     */
    private $mobile;

    /**
     * Customer constructor.
     * @param Id $id
     * @param Name $name
     * @param Email $email
     * @param string $mobile
     */
    public function __construct(Id $id, Name $name, Email $email, string $mobile)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->addresses = new ArrayCollection();
        $this->mobile = $mobile;
    }

    public function attachAddress(string $address, string $city, string $state, string $zipCode, string $country): void
    {
        $this->addresses->add(new Address($address, $city, $state, $zipCode, $country, $this));
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Email
     */
    public function getEmail(): Email
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @return Address[]
     */
    public function getAddresses(): array
    {
        return $this->addresses->toArray();
    }
}