<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Customer\Address;

use App\Model\Product\Entity\Customer\Customer;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_customer_address")
 */
class Address
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $state;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $zipCode;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $country;

    /**
     * @var Customer
     * @ORM\ManyToOne(targetEntity="App\Model\Product\Entity\Customer\Customer", inversedBy="addresses")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $customer;

    /**
     * Address constructor.
     * @param string $address
     * @param string $city
     * @param string $state
     * @param string $zipCode
     * @param string $country
     * @param Customer $customer
     */
    public function __construct(string $address, string $city, string $state, string $zipCode, string $country, Customer $customer)
    {
        $this->address = $address;
        $this->city = $city;
        $this->state = $state;
        $this->zipCode = $zipCode;
        $this->country = $country;
        $this->customer = $customer;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param string $address
     * @param string $city
     * @param string $country
     * @return bool
     */
    public function isForAddress(string $address, string $city, string $country): bool
    {
        return $this->address === $address && $this->city === $city && $this->country === $country;
    }

    public function getSelf(): self
    {
        return $this;
    }
}