<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Order;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class OrderRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repo = $em->getRepository(Order::class);
        $this->em = $em;
    }

    /**
     * @param int $id
     * @return Order
     * @throws EntityNotFoundException
     */
    public function get(int $id): Order
    {
        /** @var Order $order */
        if (!$order = $this->repo->find($id)) {
            throw new EntityNotFoundException('Order is not found.');
        }
        return $order;
    }

    public function add(Order $order): void
    {
        $this->em->persist($order);
    }
}