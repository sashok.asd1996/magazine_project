<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Order;

use App\Model\Product\Entity\Product\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_order_order_item")
 */
class OrderItem
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="items")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $order;
    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="App\Model\Product\Entity\Product\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;
    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $quantity;
    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $subtotal;

    /**
     * OrderItem constructor.
     * @param Order $order
     * @param Product $product
     * @param int $quantity
     * @param float $subtotal
     */
    public function __construct(Order $order, Product $product, int $quantity, float $subtotal)
    {
        $this->order = $order;
        $this->product = $product;
        $this->quantity = $quantity;
        $this->subtotal = $subtotal;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getSubtotal()
    {
        return $this->subtotal;
    }

}