<?php

declare(strict_types=1);

namespace App\Model\Product\Entity\Order;

use App\Model\Product\Entity\Customer\Address\Address;
use App\Model\Product\Entity\Customer\Customer;
use App\Model\Product\Entity\Product\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_order_orders")
 */
class Order
{
    private const STATUS_ACTIVE = 'active';
    private const STATUS_DELIVERED = 'delivered';

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="App\Model\Product\Entity\Customer\Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;
    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $grandTotal;
    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $created;
    /**
     * @var string
     * @ORM\Column(type="string", length=15)
     */
    private $status;

    /**
     * @var Address
     * @ORM\ManyToOne(targetEntity="App\Model\Product\Entity\Customer\Address\Address")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     */
    private $address;

    /**
     * @var OrderItem[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="order", orphanRemoval=true, cascade={"persist"})
     */
    private $items;

    /**
     * Order constructor.
     * @param Customer $customer
     * @param float $grandTotal
     * @param \DateTimeImmutable $created
     * @param Address $address
     */
    public function __construct(Customer $customer, float $grandTotal, \DateTimeImmutable $created, Address $address)
    {
        $this->customer = $customer;
        $this->grandTotal = $grandTotal;
        $this->created = $created;
        $this->status = self::STATUS_ACTIVE;
        $this->address = $address;
        $this->items = new ArrayCollection();
    }

    public function attachItem(Product $product, int $quantity, float $subtotal): void
    {
        $this->items->add(new OrderItem($this, $product, $quantity, $subtotal));
    }

    /**
     * @return OrderItem[]
     */
    public function getItems(): array
    {
        return $this->items->toArray();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function getGrandTotal()
    {
        return $this->grandTotal;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreated(): \DateTimeImmutable
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }
}