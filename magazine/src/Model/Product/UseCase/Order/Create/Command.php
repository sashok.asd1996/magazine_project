<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Order\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $customer;

    /**
     * @var array
     * @Assert\NotBlank()
     */
    public $products;

    /**
     * @var array
     * @Assert\NotBlank()
     */
    public $quantity;

    /**
     * @var float
     * @Assert\NotBlank()
     */
    public $grandTotal;

    /**
     * @var array
     * @Assert\NotBlank()
     */
    public $subTotal;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $address;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $city;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $state;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $zipCode;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $country;

    /**
     * Command constructor.
     * @param string $customer
     */
    public function __construct(string $customer)
    {
        $this->customer = $customer;
    }
}