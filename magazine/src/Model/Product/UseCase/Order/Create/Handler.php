<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Order\Create;

use App\Model\Flusher;
use App\Model\Product\Entity\Customer\Address\Address;
use App\Model\Product\Entity\Customer\CustomerRepository;
use App\Model\Product\Entity\Customer\Id;
use App\Model\Product\Entity\Order\Order;
use App\Model\Product\Entity\Order\OrderRepository;
use App\Model\Product\Entity\Product\ProductRepository;

class Handler
{
    private $flusher;
    private $customers;
    private $orders;
    private $products;

    /**
     * Handler constructor.
     * @param Flusher $flusher
     * @param CustomerRepository $customers
     * @param OrderRepository $orders
     * @param ProductRepository $products
     */
    public function __construct(
        Flusher $flusher,
        CustomerRepository $customers,
        OrderRepository $orders,
        ProductRepository $products
    )
    {
        $this->flusher = $flusher;
        $this->customers = $customers;
        $this->orders = $orders;
        $this->products = $products;
    }

    /**
     * @param Command $command
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        $id = new Id($command->customer);
        $checkAddress = true;

        $customer = $this->customers->get($id);
        foreach ($customer->getAddresses() as $address) {
            if ($address->isForAddress($command->address, $command->city, $command->country)) {
                $order = new Order($customer, $command->grandTotal, new \DateTimeImmutable(), $address->getSelf());
                $checkAddress = false;
                break;
            }
        }
        if ($checkAddress) {
            $customer->attachAddress(
                $command->address,
                $command->city,
                $command->state,
                $command->zipCode,
                $command->country
            );
            $this->customers->add($customer);
            $address = $customer->getAddresses();
            $order = new Order($customer, $command->grandTotal, new \DateTimeImmutable(), $address[count($address) - 1]->getSelf());
        }
        foreach ($command->products as $product_id) {
            $product = $this->products->get($product_id);
            $order->attachItem($product, $command->quantity[$product_id], $command->subTotal[$product_id]);
        }

        $this->orders->add($order);
        $this->flusher->flush();
    }
}