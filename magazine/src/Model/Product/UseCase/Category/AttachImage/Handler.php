<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Category\AttachImage;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\CategoryRepository;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Handler
{
    private $flusher;
    private $category;

    /**
     * Handler constructor.
     * @param Flusher $flusher
     * @param CategoryRepository $category
     */
    public function __construct(Flusher $flusher, CategoryRepository $category)
    {
        $this->flusher = $flusher;
        $this->category = $category;
    }

    public function handle(Command $command): void
    {
        $category = $this->category->get($command->id);

        foreach ($command->images as $image) {
            $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $image->guessExtension();
            $path = $command->parameter . '/' . $category->getName();
            try {
                $image->move(
                    $path,
                    $newFilename
                );
            } catch (FileException $e) {
                throw new FileException('Error');
            }
            $category->attachImage($newFilename, $path);
        }

        $this->category->add($category);
        $this->flusher->flush();
    }
}