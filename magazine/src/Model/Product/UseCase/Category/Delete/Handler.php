<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Category\Delete;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\CategoryRepository;
use App\Model\Product\Entity\Product\ProductRepository;
use Symfony\Component\Filesystem\Filesystem;

class Handler
{
    private $categories;
    private $products;
    private $flusher;

    public function __construct(CategoryRepository $categories, ProductRepository $products, Flusher $flusher)
    {

        $this->categories = $categories;
        $this->flusher = $flusher;
        $this->products = $products;
    }

    public function handle(Command $command): void
    {
        $category = $this->categories->get($command->id);

        if ($this->products->hasByCategory($command->id)) {
            throw new \DomainException('Category is not empty.');
        }

        foreach ($category->getImages() as $image) {
            $fileSystem = new Filesystem();
            $fileSystem->remove($image->getPath() . '/' . $image->getName());
            $fileSystem->remove(str_replace('/app/public', '/app/public/media/cache/my_thumb', $image->getPath()) . '/' . $image->getName());

            if (empty(array_diff(scandir($image->getPath()), array('..', '.')))) {
                $fileSystem->remove($image->getPath());
                $fileSystem->remove(str_replace('/app/public', '/app/public/media/cache/my_thumb', $image->getPath()));
            }
        }

        $this->categories->remove($category);

        $this->flusher->flush();
    }
}