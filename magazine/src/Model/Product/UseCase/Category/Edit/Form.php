<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Category\Edit;

use App\Model\Product\Entity\Product\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', Type\TextType::class)
            ->add('sex', Type\ChoiceType::class, ['choices' => [
                'Male' => Product::SEX_MALE,
                'Female' => Product::SEX_FEMALE,
            ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
        ]);
    }
}