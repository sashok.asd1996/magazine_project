<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Category\Edit;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\CategoryRepository;

class Handler
{
    private $categories;
    private $flusher;

    /**
     * Handler constructor.
     * @param CategoryRepository $category
     * @param Flusher $flusher
     */
    public function __construct(CategoryRepository $category, Flusher $flusher)
    {
        $this->categories = $category;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function handle(Command $command): void
    {
        $category = $this->categories->get($command->id);

        $category->setName($command->name);
        $category->setSex($command->sex);

        $this->flusher->flush();
    }
}