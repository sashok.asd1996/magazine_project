<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Category\Edit;

use App\Model\Product\Entity\Product\Category;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $sex;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromCategory(Category $category): self
    {
        $command = new self($category->getId());
        $command->name = $category->getName();
        $command->sex = $category->getSex();
        return $command;
    }
}