<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Category\Create;

use App\Model\Product\Entity\Product\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', Type\TextType::class)
            ->add('sex', Type\ChoiceType::class, ['choices' => [
                'Male' => Product::SEX_MALE,
                'Female' => Product::SEX_FEMALE,
            ]])
            ->add('images', Type\FileType::class, [
                'required' => false,
                'label' => 'Upload image',
                'multiple' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Command::class,
        ));
    }
}