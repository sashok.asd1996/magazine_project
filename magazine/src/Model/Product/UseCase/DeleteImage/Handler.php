<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\DeleteImage;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\ImageRepository;
use Symfony\Component\Filesystem\Filesystem;

class Handler
{
    private $images;
    private $flusher;

    public function __construct(ImageRepository $images, Flusher $flusher)
    {

        $this->images = $images;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        $image = $this->images->get($command->id);

        $this->images->remove($image);
        $this->flusher->flush();

        $fileSystem = new Filesystem();
        $fileSystem->remove($image->getPath() . '/' . $image->getName());
        $fileSystem->remove(str_replace('/app/public', '/app/public/media/cache/my_thumb', $image->getPath()) . '/' . $image->getName());

        if (empty(array_diff(scandir($image->getPath()), array('..', '.')))) {
            $fileSystem->remove($image->getPath());
            $fileSystem->remove(str_replace('/app/public', '/app/public/media/cache/my_thumb', $image->getPath()));
        }
    }
}