<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Product\Remove;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\ProductRepository;
use Symfony\Component\Filesystem\Filesystem;

class Handler
{
    private $flusher;
    private $products;

    /**
     * Handler constructor.
     * @param Flusher $flusher
     * @param ProductRepository $products
     */
    public function __construct(Flusher $flusher, ProductRepository $products)
    {
        $this->flusher = $flusher;
        $this->products = $products;
    }

    public function handle(Command $command): void
    {
        $product = $this->products->get($command->id);
        foreach ($product->getImages() as $image) {
            $fileSystem = new Filesystem();
            $fileSystem->remove($image->getPath() . '/' . $image->getName());
            $fileSystem->remove(str_replace('/app/public', '/app/public/media/cache/my_thumb', $image->getPath()) . '/' . $image->getName());

            if (empty(array_diff(scandir($image->getPath()), array('..', '.')))) {
                $fileSystem->remove($image->getPath());
                $fileSystem->remove(str_replace('/app/public', '/app/public/media/cache/my_thumb', $image->getPath()));
            }
        }
        $this->products->remove($product);
        $this->flusher->flush();
    }

}