<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Product\AttachImage;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\ProductRepository;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Handler
{
    private $flusher;
    private $products;

    /**
     * Handler constructor.
     * @param Flusher $flusher
     * @param ProductRepository $products
     */
    public function __construct(Flusher $flusher, ProductRepository $products)
    {
        $this->flusher = $flusher;
        $this->products = $products;
    }

    public function handle(Command $command): void
    {
        $product = $this->products->get($command->id);

        foreach ($command->images as $image) {
            $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $image->guessExtension();
            $path = $command->parameter . '/' . $product->getName();
            try {
                $image->move(
                    $path,
                    $newFilename
                );
            } catch (FileException $e) {
                throw new FileException('Error');
            }
            $product->attachImage($newFilename, $path);
        }

        $this->products->add($product);
        $this->flusher->flush();
    }
}