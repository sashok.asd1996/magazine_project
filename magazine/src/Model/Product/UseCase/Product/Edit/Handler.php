<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Product\Edit;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\BrandRepository;
use App\Model\Product\Entity\Product\CategoryRepository;
use App\Model\Product\Entity\Product\ProductRepository;
use App\Model\Product\Entity\Product\SubcategoryRepository;

class Handler
{
    private $flusher;
    private $subcategory;
    private $brand;
    private $product;

    public function __construct(Flusher $flusher, SubcategoryRepository $subcategory, BrandRepository $brand, ProductRepository $product)
    {
        $this->flusher = $flusher;
        $this->subcategory = $subcategory;
        $this->brand = $brand;
        $this->product = $product;
    }

    public function handle(Command $command): void
    {
        $product = $this->product->get($command->id);
        $subcategory = $this->subcategory->get($command->subcategory);
        $brand = $this->brand->get($command->brand);

        $product->edit(
            $command->name,
            $command->description ? $command->description : '',
            $command->price,
            $command->sex,
            $command->color,
            $command->size,
            $command->season,
            $subcategory,
            $brand
        );

        $this->product->add($product);

        $this->flusher->flush();
    }
}