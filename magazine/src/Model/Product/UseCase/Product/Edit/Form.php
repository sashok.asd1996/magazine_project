<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Product\Edit;

use App\Model\Product\Entity\Product\Product;
use App\ReadModel\Product\BrandFetcher;
use App\ReadModel\Product\CategoryFetcher;
use App\ReadModel\Product\SubcategoryFetcher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints\File;

class Form extends AbstractType
{
    private $subcategory;
    private $brand;

    public function __construct(SubcategoryFetcher $subcategory, BrandFetcher $brand)
    {
        $this->subcategory = $subcategory;
        $this->brand = $brand;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', Type\TextType::class)
            ->add('price', Type\MoneyType::class)
            ->add('sex', Type\ChoiceType::class, ['choices' => [
                'Male' => Product::SEX_MALE,
                'Female' => Product::SEX_FEMALE,
            ]])
            ->add('color', Type\ChoiceType::class, ['choices' => [
                'Black' => Product::COLOR_BLACK,
                'Blue' => Product::COLOR_BLUE,
                'Purple' => Product::COLOR_PURPLE,
                'Red' => Product::COLOR_RED,
                'White' => Product::COLOR_WHITE,
                'Yellow' => Product::COLOR_YELLOW,
            ]])
            ->add('size', Type\ChoiceType::class, ['choices' => [
                'M' => Product::SIZE_M,
                'S' => Product::SIZE_S,
            ]])
            ->add('season', Type\ChoiceType::class, ['choices' => [
                'Winter' => Product::SEASON_WINTER,
                'Summer' => Product::SEASON_SUMMER,
            ]])
            ->add('subcategory', Type\ChoiceType::class, ['choices' => array_flip($this->subcategory->assoc())])
            ->add('brand', Type\ChoiceType::class, ['choices' => array_flip($this->brand->assoc())])
            ->add('description', Type\TextareaType::class, ['required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Command::class,
        ));
    }
}