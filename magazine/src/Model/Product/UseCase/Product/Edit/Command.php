<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Product\Edit;

use App\Model\Product\Entity\Product\Product;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;
    /**
     * @var string
     */
    public $description;
    /**
     * @var float
     * @Assert\NotBlank()
     */
    public $price;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $sex;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $color;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $size;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $season;
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $subcategory;
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $brand;

    /**
     * Command constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromProduct(Product $product): self
    {
        $command = new self($product->getId());
        $command->name = $product->getName();
        $command->description = $product->getDescription();
        $command->price = $product->getPrice();
        $command->sex = $product->getSex();
        $command->color = $product->getColor();
        $command->size = $product->getSize();
        $command->season = $product->getSeason();
        $command->subcategory = $product->getSubcategory()->getId();
        $command->brand = $product->getBrand()->getId();
        return $command;
    }
}