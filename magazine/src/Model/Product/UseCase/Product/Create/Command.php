<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Product\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;
    /**
     * @var string
     */
    public $description;
    /**
     * @var float
     * @Assert\NotBlank()
     */
    public $price;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $sex;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $color;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $size;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $season;
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $category;
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $subcategory;
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $brand;

    /**
     * @var array
     */
    public $images;

    public $parameter;

    /**
     * @return int|null
     */
    public function getCategory(): ?int
    {
        return $this->category;
    }
}