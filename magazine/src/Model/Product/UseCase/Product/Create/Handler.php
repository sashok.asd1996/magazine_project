<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Product\Create;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\BrandRepository;
use App\Model\Product\Entity\Product\CategoryRepository;
use App\Model\Product\Entity\Product\Image;
use App\Model\Product\Entity\Product\Product;
use App\Model\Product\Entity\Product\ProductRepository;
use App\Model\Product\Entity\Product\SubcategoryRepository;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;
    /**
     * @var BrandRepository
     */
    private $brand;
    /**
     * @var ProductRepository
     */
    private $product;
    /**
     * @var SubcategoryRepository
     */
    private $subcategory;

    /**
     * Handler constructor.
     * @param Flusher $flusher
     * @param BrandRepository $brand
     * @param ProductRepository $product
     * @param SubcategoryRepository $subcategory
     */
    public function __construct(
        Flusher $flusher,
        BrandRepository $brand,
        ProductRepository $product,
        SubcategoryRepository $subcategory)
    {
        $this->flusher = $flusher;
        $this->brand = $brand;
        $this->product = $product;
        $this->subcategory = $subcategory;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $subcategory = $this->subcategory->get($command->subcategory);
        $brand = $this->brand->get($command->brand);

        $product = new Product(
            $command->name,
            $command->price,
            $command->sex,
            $command->color,
            $command->size,
            $command->season,
            $subcategory,
            $brand,
            $command->description
        );

        if ($images = $command->images) {
            foreach ($images as $image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $image->guessExtension();
                $path = $command->parameter . '/' . $product->getName();
                try {
                    $image->move(
                        $path,
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new FileException('Error');
                }
                $product->attachImage($newFilename, $path);
            }
        }
        $this->product->add($product);

        $this->flusher->flush();
    }
}