<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Product\Create;

use App\Model\Product\Entity\Product\Product;
use App\ReadModel\Product\BrandFetcher;
use App\ReadModel\Product\CategoryFetcher;
use App\ReadModel\Product\SubcategoryFetcher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints\File;

class Form extends AbstractType
{
    private $category;
    private $brand;
    private $subcategory;

    public function __construct(CategoryFetcher $category, BrandFetcher $brand, SubcategoryFetcher $subcategory)
    {
        $this->category = $category;
        $this->brand = $brand;
        $this->subcategory = $subcategory;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', Type\TextType::class)
            ->add('price', Type\MoneyType::class)
            ->add('sex', Type\ChoiceType::class, ['choices' => [
                'Male' => Product::SEX_MALE,
                'Female' => Product::SEX_FEMALE,
            ]])
            ->add('color', Type\ChoiceType::class, ['choices' => [
                'Black' => Product::COLOR_BLACK,
                'Blue' => Product::COLOR_BLUE,
                'Purple' => Product::COLOR_PURPLE,
                'Red' => Product::COLOR_RED,
                'White' => Product::COLOR_WHITE,
                'Yellow' => Product::COLOR_YELLOW,
            ]])
            ->add('size', Type\ChoiceType::class, ['choices' => [
                'M' => Product::SIZE_M,
                'S' => Product::SIZE_S,
            ]])
            ->add('season', Type\ChoiceType::class, ['choices' => [
                'Winter' => Product::SEASON_WINTER,
                'Summer' => Product::SEASON_SUMMER,
            ]])
            ->add('category', Type\ChoiceType::class, ['choices' => array_flip($this->category->assoc())])
            ->add('brand', Type\ChoiceType::class, ['choices' => array_flip($this->brand->assoc())])
            ->add('description', Type\TextareaType::class, ['required' => false])
            ->add('images', Type\FileType::class, [
                'required' => false,
                'label' => 'Upload image',
                'multiple' => true,
                ]);

        $formModifier = function (FormInterface $form, int $category = 0) {
            $category = $category === null ? 0 : $category;
            $form->add('subcategory', Type\ChoiceType::class, [
                'choices' => array_flip($this->subcategory->getAvailableSubcategory($category))
            ]);
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();

                $category = $data->getCategory() === null ? 0 : $data->getCategory();

                $formModifier($event->getForm(), $category);
            }
        );

        $builder->get('category')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $category = $event->getForm()->getData();

                $formModifier($event->getForm()->getParent(), $category);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Command::class,
        ));
    }
}