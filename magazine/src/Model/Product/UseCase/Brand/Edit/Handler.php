<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Brand\Edit;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\BrandRepository;
use App\Model\Product\Entity\Product\CategoryRepository;

class Handler
{
    private $brand;
    private $flusher;

    /**
     * Handler constructor.
     * @param BrandRepository $brand
     * @param Flusher $flusher
     */
    public function __construct(BrandRepository $brand, Flusher $flusher)
    {
        $this->brand = $brand;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $category = $this->brand->get($command->id);

        $category->setName($command->name);

        $this->flusher->flush();
    }
}