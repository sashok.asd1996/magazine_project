<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Brand\Edit;

use App\Model\Product\Entity\Product\Brand;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromBrand(Brand $brand): self
    {
        $command = new self($brand->getId());
        $command->name = $brand->getName();
        return $command;
    }
}