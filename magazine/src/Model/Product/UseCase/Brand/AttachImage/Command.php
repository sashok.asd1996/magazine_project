<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Brand\AttachImage;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var int
     */
    public $id;

    /**
     * @Assert\NotBlank()
     */
    public $images;

    /**
     * @var string
     */
    public $parameter;

    /**
     * Command constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }
}