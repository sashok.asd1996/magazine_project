<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Brand\AttachImage;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\BrandRepository;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Handler
{
    private $flusher;
    private $brand;

    /**
     * Handler constructor.
     * @param Flusher $flusher
     * @param BrandRepository $brand
     */
    public function __construct(Flusher $flusher, BrandRepository $brand)
    {
        $this->flusher = $flusher;
        $this->brand = $brand;
    }

    public function handle(Command $command): void
    {
        $brand = $this->brand->get($command->id);

        foreach ($command->images as $image) {
            $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $image->guessExtension();
            $path = $command->parameter . '/' . $brand->getName();
            try {
                $image->move(
                    $path,
                    $newFilename
                );
            } catch (FileException $e) {
                throw new FileException('Error');
            }
            $brand->attachImage($newFilename, $path);
        }

        $this->brand->add($brand);
        $this->flusher->flush();
    }
}