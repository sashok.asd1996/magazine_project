<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Brand\Delete;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\BrandRepository;
use App\Model\Product\Entity\Product\ProductRepository;
use Symfony\Component\Filesystem\Filesystem;

class Handler
{
    private $brand;
    private $products;
    private $flusher;

    public function __construct(BrandRepository $brand, ProductRepository $products, Flusher $flusher)
    {

        $this->brand = $brand;
        $this->flusher = $flusher;
        $this->products = $products;
    }

    public function handle(Command $command): void
    {
        $brand = $this->brand->get($command->id);

        if ($this->products->hasByBrand($command->id)) {
            throw new \DomainException('Brand is not empty.');
        }

        foreach ($brand->getImages() as $image) {
            $fileSystem = new Filesystem();
            $fileSystem->remove($image->getPath() . '/' . $image->getName());
            $fileSystem->remove(str_replace('/app/public', '/app/public/media/cache/my_thumb', $image->getPath()) . '/' . $image->getName());

            if (empty(array_diff(scandir($image->getPath()), array('..', '.')))) {
                $fileSystem->remove($image->getPath());
                $fileSystem->remove(str_replace('/app/public', '/app/public/media/cache/my_thumb', $image->getPath()));
            }
        }

        $this->brand->remove($brand);

        $this->flusher->flush();
    }
}