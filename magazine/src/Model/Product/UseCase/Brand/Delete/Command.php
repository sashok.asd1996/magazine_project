<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Brand\Delete;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    public $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}