<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Brand\Create;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\Brand;
use App\Model\Product\Entity\Product\BrandRepository;
use App\Model\Product\Entity\Product\Category;

class Handler
{
    private $flusher;
    private $brand;

    /**
     * Handler constructor.
     * @param Flusher $flusher
     * @param BrandRepository $brand
     */
    public function __construct(Flusher $flusher, BrandRepository $brand)
    {
        $this->flusher = $flusher;
        $this->brand = $brand;
    }

    public function handle(Command $command):void
    {
        $brand = new Brand($command->name);
        $this->brand->add($brand);
        $this->flusher->flush();
    }
}