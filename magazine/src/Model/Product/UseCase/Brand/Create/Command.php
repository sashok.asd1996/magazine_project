<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Brand\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;
}