<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Subcategory\Delete;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\ProductRepository;
use App\Model\Product\Entity\Product\SubcategoryRepository;
use Symfony\Component\Filesystem\Filesystem;

class Handler
{
    private $subcategories;
    private $products;
    private $flusher;

    public function __construct(SubcategoryRepository $subcategories, ProductRepository $products, Flusher $flusher)
    {

        $this->subcategories = $subcategories;
        $this->flusher = $flusher;
        $this->products = $products;
    }

    public function handle(Command $command): void
    {
        $subcategory = $this->subcategories->get($command->id);

        if ($this->products->hasBySubcategory($command->id)) {
            throw new \DomainException('Subcategory is not empty.');
        }

        foreach ($subcategory->getImages() as $image) {
            $fileSystem = new Filesystem();
            $fileSystem->remove($image->getPath() . '/' . $image->getName());
            $fileSystem->remove(str_replace('/app/public', '/app/public/media/cache/my_thumb', $image->getPath()) . '/' . $image->getName());

            if (empty(array_diff(scandir($image->getPath()), array('..', '.')))) {
                $fileSystem->remove($image->getPath());
                $fileSystem->remove(str_replace('/app/public', '/app/public/media/cache/my_thumb', $image->getPath()));
            }
        }

        $this->subcategories->remove($subcategory);

        $this->flusher->flush();
    }
}