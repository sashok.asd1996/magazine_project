<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Subcategory\AttachImage;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\SubcategoryRepository;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Handler
{
    private $flusher;
    private $subcategory;

    /**
     * Handler constructor.
     * @param Flusher $flusher
     * @param SubcategoryRepository $subcategory
     */
    public function __construct(Flusher $flusher, SubcategoryRepository $subcategory)
    {
        $this->flusher = $flusher;
        $this->subcategory = $subcategory;
    }

    public function handle(Command $command): void
    {
        $subcategory = $this->subcategory->get($command->id);

        foreach ($command->images as $image) {
            $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $image->guessExtension();
            $path = $command->parameter . '/' . $subcategory->getCategory()->getName() . '/' . $subcategory->getName();
            try {
                $image->move(
                    $path,
                    $newFilename
                );
            } catch (FileException $e) {
                throw new FileException('Error');
            }
            $subcategory->attachImage($newFilename, $path);
        }

        $this->subcategory->add($subcategory);
        $this->flusher->flush();
    }
}