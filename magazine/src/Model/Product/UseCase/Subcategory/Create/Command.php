<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Subcategory\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $category;

    /**
     * @var array
     */
    public $images;

    public $parameter;

    /**
     * Command constructor.
     * @param int $category
     */
    public function __construct(int $category)
    {
        $this->category = $category;
    }
}