<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Subcategory\Create;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\Category;
use App\Model\Product\Entity\Product\CategoryRepository;
use App\Model\Product\Entity\Product\Subcategory;
use App\Model\Product\Entity\Product\SubcategoryRepository;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Handler
{
    private $flusher;
    private $subcategory;
    private $category;

    /**
     * Handler constructor.
     * @param Flusher $flusher
     * @param SubcategoryRepository $subcategory
     * @param CategoryRepository $category
     */
    public function __construct(Flusher $flusher, SubcategoryRepository $subcategory, CategoryRepository $category)
    {
        $this->flusher = $flusher;
        $this->subcategory = $subcategory;
        $this->category = $category;
    }

    public function handle(Command $command):void
    {
        $category = $this->category->get($command->category);
        $subcategory = new Subcategory($command->name, $category);
        if ($images = $command->images) {
            foreach ($images as $image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $image->guessExtension();
                $path = $command->parameter . '/' . $category->getName() . '/' . $subcategory->getName();
                try {
                    $image->move(
                        $path,
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new FileException('Error');
                }
                $subcategory->attachImage($newFilename, $path);
            }
        }
        $this->subcategory->add($subcategory);
        $this->flusher->flush();
    }
}