<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Subcategory\Edit;

use App\ReadModel\Product\CategoryFetcher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

class Form extends AbstractType
{
    private $category;

    public function __construct(CategoryFetcher $category)
    {
        $this->category = $category;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', Type\TextType::class)
            ->add('category', Type\ChoiceType::class, ['choices' => array_flip($this->category->assoc())]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
        ]);
    }
}