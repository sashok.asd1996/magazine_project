<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Subcategory\Edit;

use App\Model\Flusher;
use App\Model\Product\Entity\Product\CategoryRepository;
use App\Model\Product\Entity\Product\SubcategoryRepository;

class Handler
{
    private $subcategories;
    private $flusher;
    private $categories;

    /**
     * Handler constructor.
     * @param SubcategoryRepository $subcategory
     * @param CategoryRepository $categories
     * @param Flusher $flusher
     */
    public function __construct(SubcategoryRepository $subcategory, Flusher $flusher, CategoryRepository $categories)
    {
        $this->subcategories = $subcategory;
        $this->flusher = $flusher;
        $this->categories = $categories;
    }

    /**
     * @param Command $command
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function handle(Command $command): void
    {
        $category = $this->categories->get($command->category);
        $subcategory = $this->subcategories->get($command->id);

        $subcategory->setName($command->name);
        $subcategory->setCategory($category);

        $this->flusher->flush();
    }
}