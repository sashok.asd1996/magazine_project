<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Subcategory\Edit;

use App\Model\Product\Entity\Product\Category;
use App\Model\Product\Entity\Product\Subcategory;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $category;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromSubcategory(Subcategory $subcategory): self
    {
        $command = new self($subcategory->getId());
        $command->name = $subcategory->getName();
        $command->category = $subcategory->getCategory()->getId();
        return $command;
    }
}