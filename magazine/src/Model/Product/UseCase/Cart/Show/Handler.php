<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Cart\Show;

use App\Model\Product\Entity\Product\ProductRepository;

class Handler
{
    private $products;

    /**
     * Handler constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->products = $productRepository;
    }

    public function handle(array $productsInSession): array
    {
        $products = array();
        $grandTotal = 0;
        $subTotal = array();
        $count = array_count_values($productsInSession);
        $uniqueProductsInSession = array_unique($productsInSession);
        sort($uniqueProductsInSession);
        foreach ($uniqueProductsInSession as $product_id) {
            $product = $this->products->get($product_id);
            array_push($products, $product);
            $subTotal[$product_id] = $product->getPrice() * $count[$product_id];
            $grandTotal += $subTotal[$product_id];
        }
        return array(
            'products' => $products,
            'count' => $count,
            'grand_total' => $grandTotal,
            'sub_total' => $subTotal,
        );
    }
}