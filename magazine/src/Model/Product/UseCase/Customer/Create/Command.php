<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Customer\Create;

use App\Model\Product\Entity\Customer\Customer;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $id;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public $email;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $firstName;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $lastName;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $address;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $city;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $state;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $zipCode;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $country;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $mobile;

    /**
     * Command constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function fromCustomer(Customer $customer): self
    {
        $command = new self($customer->getId()->getValue());
        $command->email = $customer->getEmail()->getValue();
        $command->firstName = $customer->getName()->getFirst();
        $command->lastName = $customer->getName()->getLast();
        $address = $customer->getAddresses();
        $address = $address[count($address) - 1];
        $command->address = $address->getAddress();
        $command->city = $address->getCity();
        $command->state = $address->getState();
        $command->zipCode = $address->getZipCode();
        $command->country = $address->getCountry();
        $command->mobile = $customer->getMobile();

        return $command;
    }
}