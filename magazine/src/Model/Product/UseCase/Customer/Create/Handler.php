<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Customer\Create;

use App\Model\Flusher;
use App\Model\Product\Entity\Customer\Customer;
use App\Model\Product\Entity\Customer\CustomerRepository;
use App\Model\Product\Entity\Customer\Email;
use App\Model\Product\Entity\Customer\Id;
use App\Model\Product\Entity\Customer\Name;

class Handler
{
    private $flusher;
    private $customers;

    /**
     * Handler constructor.
     * @param Flusher $flusher
     * @param CustomerRepository $customers
     */
    public function __construct(
        Flusher $flusher,
        CustomerRepository $customers
    )
    {
        $this->flusher = $flusher;
        $this->customers = $customers;
    }

    /**
     * @param Command $command
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        $id = new Id($command->id);

        $customer = new Customer(
            $id,
            new Name(
                $command->firstName,
                $command->lastName
            ),
            new Email($command->email),
            $command->mobile);

        $customer->attachAddress($command->address, $command->city, $command->state, $command->zipCode, $command->country);

        $this->customers->add($customer);
        $this->flusher->flush();
    }
}