<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Customer\Create;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', Type\TextType::class, ['label' => 'First Name'])
            ->add('lastName', Type\TextType::class, ['label' => 'Last Name'])
            ->add('email', Type\EmailType::class, ['label' => 'Email'])
            ->add('address', Type\TextType::class, ['label' => 'Address'])
            ->add('city', Type\TextType::class, ['label' => 'City'])
            ->add('state', Type\TextType::class, ['label' => 'State'])
            ->add('zipCode', Type\TextType::class, ['label' => 'Zip Code'])
            ->add('country', Type\TextType::class, ['label' => 'Country'])
            ->add('mobile', Type\TextType::class, ['label' => 'Mobile']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Command::class,
        ));
    }
}