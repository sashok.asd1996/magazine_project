<?php

declare(strict_types=1);

namespace App\Model\User\UseCase\Name;

use App\Model\User\Entity\User\Id;
use App\Model\User\Entity\User\Name;
use App\Model\User\Entity\User\UserRepository;
use App\Model\Flusher;

class Handler
{
    private $users;
    private $flusher;

    /**
     * Handler constructor.
     * @param $users
     * @param $flusher
     */
    public function __construct(UserRepository $users, Flusher $flusher)
    {
        $this->users = $users;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function handle(Command $command): void
    {
        $user = $this->users->get(New Id($command->id));

        $user->changeName(new Name(
            $command->firstName,
            $command->lastName
        ));

        $this->flusher->flush();
    }

}