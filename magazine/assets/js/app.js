require('../css/app.scss');

require('bootstrap');
require('jquery');
require('@coreui/coreui');
const $ = require('jquery');
global.$ = global.jQuery = $;

global.conn = new WebSocket('ws://localhost:8081');

conn.onopen = function () {
    if (!document.querySelector('#sessionAdmin')) {
        conn.send(JSON.stringify({
            'userId': document.querySelector('#session').value,
            'type': 'refreshChat',
        }));
    } else {
        conn.send(JSON.stringify({
            'adminId': document.querySelector('#sessionAdmin').value,
            'type': 'refreshChat',
        }));
    }
};
let countMessage = 0;
conn.onmessage = function (e) {
    let json = JSON.parse(e.data);
    switch (json.type) {
        case 'shop':
            let box = document.querySelector('.show-place-order');
            let href = Array();
            json.product_name.forEach(function (element, index) {
                href.push('<a href="' + json.product_href[index] + '">' + element + '</a>');
            });
            box.innerHTML = box.innerHTML + '<p>' + json.user + ' bought ' + href + '</p>';
            box.style.display = "block";
            box.style.marginLeft = document.documentElement.clientWidth - 600 + "px";
            if (box.children.length > 5) {
                box.firstChild.remove();
            }
            setTimeout(function () {
                box.firstChild.remove();
                if (box.innerHTML === "") {
                    box.style.marginLeft = "100%";
                    box.style.display = "none";
                }
            }, 12000);
            break;
        case 'chatAdmin':
            let text = json.text;
            let userId = json.userId;
            let who = json.who;
            let checkUser = false;
            let messages = document.querySelector('.messages');
            let message = document.createElement('div');
            message.className = 'message';
            if (document.querySelector('.user-in-chat'))
            {
                let user_in_chat = document.querySelectorAll('.user-in-chat');
                user_in_chat.forEach(function (element) {
                    if (element.id === userId) {
                        checkUser = true;
                        countMessage++;
                        element.innerHTML = userId.slice(0, 10) + '(' + countMessage + ')' ;
                    }
                });
            }
            let name_users = document.querySelector('.name-users');
            let pName = document.createElement('p');
            if (who === 'admin') {
                if (document.querySelector('#sessionAdmin')) {
                    let message_from_me = document.createElement('div');
                    let p = document.createElement('p');
                    message_from_me.className = 'message-from-me';
                    p.innerHTML = 'You';
                    message_from_me.appendChild(p);
                    message_from_me.append(text);
                    message.appendChild(message_from_me);
                } else {
                    let message_to_me = document.createElement('div');
                    let p = document.createElement('p');
                    message_to_me.className = 'message-to-me';
                    p.innerHTML = 'Admin';
                    message_to_me.appendChild(p);
                    message_to_me.append(text);
                    message.appendChild(message_to_me);
                }
                $.ajax({
                    url: '/chat/add',
                    type: "POST",
                    data: JSON.stringify({
                        'text': text,
                        'from': 'admin',
                        'to': json.toUserId
                    })
                });
            }
            if (who === 'user') {
                if (!checkUser && document.querySelector('#sessionAdmin')) {
                    pName.className = 'user-in-chat';
                    pName.id = userId;
                    pName.innerHTML = userId.slice(0, 10);
                    name_users.append(pName);
                    $('.user-in-chat').click(userInChat);
                }
                if (document.querySelector('#session') && document.querySelector('#session').value === userId) {
                    let message_from_me = document.createElement('div');
                    let p = document.createElement('p');
                    message_from_me.className = 'message-from-me';
                    p.innerHTML = 'You';
                    message_from_me.appendChild(p);
                    message_from_me.append(text);
                    message.appendChild(message_from_me);
                } else {
                    let user_in_chat = document.querySelectorAll('.user-in-chat');
                    user_in_chat.forEach(function (element) {
                        if (element.classList.contains('active') && element.id === userId){
                            let message_to_me = document.createElement('div');
                            let p = document.createElement('p');
                            message_to_me.className = 'message-to-me';
                            p.innerHTML = userId;
                            message_to_me.appendChild(p);
                            message_to_me.append(text);
                            message.appendChild(message_to_me);
                        }
                    });
                }
                $.ajax({
                    url: '/chat/add',
                    type: "POST",
                    data: JSON.stringify({
                        'text': text,
                        'from': userId,
                        'to': 'admin'
                    })
                });
            }
            messages.appendChild(message);
            let scroll = $('.messages');
            scroll.scrollTop(scroll.prop("scrollHeight"));
            break;
    }
};

$(document).ready(function () {
    $('.open-chat').click(function () {
        let chat = document.querySelector('.sticky-chat');
        let close = document.querySelector('.close-chat');
        chat.style.bottom = "0";
        close.style.display = "inline-block";
    });
    $('.close-chat').click(function () {
        let chat = document.querySelector('.sticky-chat');
        let close = document.querySelector('.close-chat');
        chat.style.bottom = "-465px";
        close.style.display = "none";
    });

    $('#place-order').click(function () {
        let product = document.querySelectorAll('.product-name');
        let product_name = Array();
        let product_href = Array();
        product.forEach(function (element) {
            product_name.push(element.innerHTML);
            product_href.push(element.dataset.href);
        });
        let user = product[0].dataset.user;
        conn.send(JSON.stringify({
            'type': 'shop',
            'product_name': product_name,
            'product_href': product_href,
            'user': user
        }));
    });

    $('#send-message-to-admin').click(function () {
        let text = document.querySelector('#text-message').value;
        let userId = document.querySelector('#session').value;
        conn.send(JSON.stringify({
            'type': 'chatAdmin',
            'who': 'user',
            'text': text,
            'userId': userId,
        }));
    });

    $('#send-message-from-admin').click(function () {
        let text = document.querySelector('#text-message').value;
        let toUserId = document.querySelector('.message-to-me p').innerHTML;
        conn.send(JSON.stringify({
            'type': 'chatAdmin',
            'who': 'admin',
            'text': text,
            'toUserId': toUserId,
        }));
    });

    $('.user-in-chat').click(userInChat);
});

function userInChat() {
    let user_id = $(this).attr("id");
    $(this).html(user_id.slice(0,10));
    let user_in_chat = document.querySelectorAll('.user-in-chat');
    user_in_chat.forEach(function (element) {
        element.classList.remove('active');
    });
    $(this).addClass('active');
    $.ajax({
        url: '/chat/user',
        type: "POST",
        data: JSON.stringify({
            'user_id': user_id,
        }),
        success: function (data) {
            let messages = document.querySelector('.messages');
            messages.innerHTML = '';
            for (let index in data) {
                if (data.hasOwnProperty(index)) {
                    let message = document.createElement('div');
                    message.className = 'message';
                    let text = data[index].text;
                    let from = data[index].from;
                    if (from === 'admin') {
                        let message_from_me = document.createElement('div');
                        let p = document.createElement('p');
                        message_from_me.className = 'message-from-me';
                        p.innerHTML = 'You';
                        message_from_me.appendChild(p);
                        message_from_me.append(text);
                        message.appendChild(message_from_me);
                    } else {
                        let message_to_me = document.createElement('div');
                        let p = document.createElement('p');
                        message_to_me.className = 'message-to-me';
                        p.innerHTML = from;
                        message_to_me.appendChild(p);
                        message_to_me.append(text);
                        message.appendChild(message_to_me);
                    }
                    messages.appendChild(message);
                }
            }
            let scroll = $('.messages');
            scroll.scrollTop(scroll.prop("scrollHeight"));
        }
    });
}