$('.add_to_cart').click(function () {
    let $product_id = $(this).attr("id");
    $.ajax({
        url: '/shop/cart/add',
        type: "POST",
        data: $product_id,
        success: function (data) {
            $.ajax({
                url: "",
                context: document.body,
                success: function(html){
                    $('body').html(html);
                    $('#action' + $product_id).append('<li class="cartMessage">' + data + '</li>');
                    setTimeout(
                        function()
                        {
                            $('.cartMessage').remove();
                        }, 1200);
                }
            });
        }
    });
});

$('.remove_from_cart').click(function () {
    let $product_id = $(this).attr("id");
    $.ajax({
        url: '/shop/cart/remove',
        type: "POST",
        data: $product_id,
        success: function (data) {
            $.ajax({
                url: "",
                context: document.body,
                success: function(html){
                    $('body').html(html);
                    $('#action' + $product_id).append('<li class="cartMessage">' + data + '</li>');
                    setTimeout(
                        function()
                        {
                            $('.cartMessage').remove();
                        }, 1200);
                }
            });
        }
    });
});