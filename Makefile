up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear manager-clear docker-pull docker-build docker-up manager-init
test: manager-test

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

manager-init: manager-composer-install manager-assets-install manager-migrations manager-fixtures manager-ready


manager-composer-install:
	docker-compose run --rm manager-php-cli composer install

manager-assets-install:
	docker-compose run --rm manager-node yarn install
	docker-compose run --rm manager-node npm rebuild node-sass
	docker-compose run --rm manager-node yarn encore dev
	sleep 30

manager-clear:
    docker run --rm -v ${PWD}/manager:/app --workdir=/app alpine rm -f .ready

manager-migrations:
	docker-compose run --rm manager-php-cli php bin/console doctrine:migrations:migrate --no-interaction

manager-fixtures:
	docker-compose run --rm manager-php-cli php bin/console doctrine:fixtures:load --no-interaction

manager-ready:
    docker run --rm -v ${PWD}/manager:/app --workdir=/app alpine touch .ready

manager-test:
	docker-compose run --rm manager-php-cli bin/phpunit --bootstrap vendor/autoload.php tests/Unit

